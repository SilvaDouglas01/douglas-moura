import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class NumerosTest
{
    @Test
    public void CalcularMediaSeguinte5fatores(){
        double[] entrada = new double[ ] { 1.0, 3.0, 5.0, 1.0, -10.0 };
        Numeros numeros = new Numeros(entrada);
        double[] media = new double[ ] { 2.0, 4.0, 3.0, -4.5 };
        Numeros numeros1 = new Numeros(media);
        assertArrayEquals(media, numeros.calcularMediaSeguinte(), 0.1);
    }
    
    @Test
    public void CalcularMediaSeguinte3fatores(){
        double[] entrada = new double[ ] { 9.0, 3.0, 7.0 };
        Numeros numeros = new Numeros(entrada);
        double[] media = new double[ ] { 6.0, 5.0 };
        Numeros numeros1 = new Numeros(media);
        assertArrayEquals(media, numeros.calcularMediaSeguinte(), 0.1);
    }
    
    @Test
    public void calcularMediaSeguinte1Fator(){
        double[] entrada = new double[] {2};
        Numeros numeros = new Numeros(entrada);
        double[] media = new double[ ] {2};
        Numeros numeros1 = new Numeros(media);
        assertArrayEquals(media, numeros.calcularMediaSeguinte(), 0.1);
    }
    
    @Test
    public void calcularMediaSeguinteSemFatores(){
        double[] entrada = new double[] {};
        Numeros numeros = new Numeros(entrada);
        double[] media = new double[ ] {};
        Numeros numeros1 = new Numeros(media);
        assertEquals(media.length, numeros.getSize());
    }
}
