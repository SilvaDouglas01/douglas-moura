
public class Numeros
{
    double numeros[];
    
    public Numeros(double numerosInformados[]){
        this.numeros = numerosInformados;
    }
    
    public double[] calcularMediaSeguinte(){
        double mediaSeguinte[] = new double[this.numeros.length-1];
        if (numeros.length==1 || numeros.length==0){
            return this.numeros;
        }
        for(int i = 0; i<mediaSeguinte.length; i++){
            mediaSeguinte[i] = (numeros[i]+numeros[i+1])/2;
        }
        return mediaSeguinte;
    }
    
    public int getSize(){
        return this.numeros.length;
    }
}
