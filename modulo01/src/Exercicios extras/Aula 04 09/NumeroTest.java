import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class NumeroTest
{
    @Test 
    public void testarImpar(){
        Numero numero1 = new Numero(5);
        assertEquals(true, numero1.impar());
        Numero numero2 = new Numero(27);
        assertEquals(true, numero2.impar());
        Numero numero3 = new Numero(2018);
        assertEquals(false, numero3.impar());
    }
    
    @Test
    public void verificarSomaDivisivel(){
        Numero numero1 = new Numero(9);
        assertEquals(true, numero1.verificarSomaDivisivel(1892376));
        Numero numero2 = new Numero(3);
        assertEquals(false, numero2.verificarSomaDivisivel(17));
    }
}
