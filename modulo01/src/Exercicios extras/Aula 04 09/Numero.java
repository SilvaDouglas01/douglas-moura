
public class Numero
{
    private int numero;
    
    public Numero(int numero){
        this.numero = numero;
    }
    
    public boolean impar(){
        if ((this.numero%2)==0)
            return false;
        return true;
    }
    
    public boolean verificarSomaDivisivel(int numeroInformado){
        String digitos = String.valueOf(numeroInformado);
        int somaDigitos=0;
        for (int x=0; x<digitos.length(); x++){
            somaDigitos+= Integer.parseInt(String.valueOf(digitos.charAt(x)));
        }
        if ((somaDigitos==0) || ((somaDigitos%numero)==0))
            return true;
        return false;
    }
}
