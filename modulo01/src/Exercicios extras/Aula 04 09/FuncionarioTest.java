import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class FuncionarioTest
{
    @Test
    public void calcularLucro(){
        Funcionario func1 = new Funcionario("Capitão Caverna", 500, 1000);
        assertEquals(585, func1.getLucro(), 0.1);
        Funcionario func2 = new Funcionario("EuDouODougras", 500, 7840.5);
        assertEquals(1508.4675, func2.getLucro(), 0.1);
    }
}
