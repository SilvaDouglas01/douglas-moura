public class Funcionario
{
    private String nome;
    private double salario, totalVendasMes;
    
    public Funcionario(String nomeInformado, double salarioInformado, double totalVendasMesInformado){
        this.salario = salarioInformado;
        this.nome = nomeInformado;
        this.totalVendasMes = totalVendasMesInformado;
    }
    
    public double getLucro(){
        return(this.salario*0.9)+((totalVendasMes*0.9)*0.15);
    }
}
