import java.util.*;
<<<<<<< HEAD
public class ExercitoDeElfos implements Estrategias
{
    private static final ArrayList<Class> ELFOS_VALIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class, 
            ElfoNoturno.class
        )
    );
    private HashMap<String, Elfo> elfosVivos = new HashMap<>();
    private HashMap<String, Elfo> elfosMortos = new HashMap<>();
    private HashMap<Status, HashMap<String, Elfo>> estadoDeVida = new HashMap<>();
    public ExercitoDeElfos(){
        estadoDeVida.put(Status.VIVO, elfosVivos);
        estadoDeVida.put(Status.MORTO, elfosMortos);
    }
    
    public void alistarElfo(Elfo elfo){
        if(ELFOS_VALIDOS.contains(elfo.getClass())){
            this.estadoDeVida.get(elfo.getStatus()).put(elfo.getNome(), elfo);
            elfo.setExercito(this);
        }
    }
    
    public HashMap<String, Elfo> buscar(Status status){
        return this.estadoDeVida.get(status);
    }
    
    public List<Elfo> noturnosPorUltimo(List<Elfo> atacantes){
        List <Elfo> ordenados = new ArrayList<>();
        for(int i = 0; i<atacantes.size();i++){
            if (atacantes.get(i).getStatus().equals(Status.VIVO) && atacantes.get(i).getClass().equals(ElfoVerde.class))
                ordenados.add(atacantes.get(i));
        }
        for(int i = 0; i<atacantes.size();i++){
            if (atacantes.get(i).getStatus().equals(Status.VIVO) && atacantes.get(i).getClass().equals(ElfoNoturno.class))
                ordenados.add(atacantes.get(i));
        }
        return ordenados;
    }
    
    public List<Elfo> ataqueIntercalado(List<Elfo> atacantes){
        List <Elfo> verdes = new ArrayList<>();
        List <Elfo> noturnos = new ArrayList<>();
        List <Elfo> ordenados = new ArrayList<>();
        for(int i = 0; i<atacantes.size();i++){
            if(atacantes.get(i).getClass().equals(ElfoNoturno.class))
                noturnos.add(atacantes.get(i));
            else if(atacantes.get(i).getClass().equals(ElfoVerde.class))
                verdes.add(atacantes.get(i));
        }
        for(int i = 0; i < verdes.size();i++){
            ordenados.add(verdes.get(i));
            ordenados.add(noturnos.get(i));
        }
        return ordenados; 
    }
    protected void moverElfoParaMorto(Elfo elfo){
        this.estadoDeVida.get(Status.VIVO).remove(elfo.getNome());
        this.estadoDeVida.get(Status.MORTO).put(elfo.getNome(), elfo);
    }
    
    public HashMap<String, Elfo> getAlistados(){
        HashMap<String, Elfo> alistados = new HashMap<>();
        alistados.putAll(this.estadoDeVida.get(Status.MORTO));
        alistados.putAll(this.estadoDeVida.get(Status.VIVO));
        return alistados;
    }
}
=======

public class ExercitoDeElfos {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class
            )
        );
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); 

    public void alistar(Elfo elfo) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if (podeAlistar) {
            elfos.add(elfo);
            ArrayList<Elfo> elfosDoStatus = porStatus.get(elfo.getStatus());
            if (elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> buscar(Status status) {
        return this.porStatus.get(status);
    }

    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
}

>>>>>>> master
