import java.util.*;
<<<<<<< HEAD
public class ElfoDaLuz extends Elfo
{
    private int qtdAtaques=0;
    private static final int QTD_VIDA_GANHA=10;
    private final ArrayList<String> DESCRICOES_INVALIDAS = new ArrayList<>(Arrays.asList("Espada de Galvorn"));
    
    public ElfoDaLuz(String nome){
        super(nome);
        this.ganharItem(new ItemImperdivel("Espada de Galvorn", 1));
        QTD_DANO = 21;
    }
    
    private void ganharVida(){
        this.setVida(this.vida+QTD_VIDA_GANHA);
    }
    
    public void atacarComEspada(Dwarf vitima){
        if(qtdAtaques%2==0)
            this.perderVida();
        else
            this.ganharVida();
        vitima.perderVida();
        this.qtdAtaques++;
    }
    
    public void perderItem(Item item) {
        boolean descricaoInvalida = DESCRICOES_INVALIDAS.contains(item.getDescricao());
        if(!descricaoInvalida)
            super.perderItem(item);
    }
}
=======

public class ElfoDaLuz extends Elfo {
    private int qtdAtaques = 0;
    private static final double QTD_VIDA_GANHA = 10;

    private static final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
                "Espada de Galvorn"
            ));

    public ElfoDaLuz(String nome) {
        super(nome);
        QTD_DANO = 21;
        super.ganharItem(new ItemImperdivel(DESCRICOES_OBRIGATORIAS.get(0), 1));
    }

    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }

    private void ganharVida() {
        vida += ElfoDaLuz.QTD_VIDA_GANHA;
    }

    @Override
    public void perderItem(Item item) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder) {
            super.perderItem(item);
        }
    }

    private Item getEspada() {
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }

    public void atacarComEspada(Dwarf dwarf) {
        Item espada = getEspada();
        // esse if nao e' mais necessario desde que corrigimos com o itemimperdivel, mas deixei aqui pra ficar claro (para aqueles que nao implementaram o cenario de testes que alterava a quantidade)
        if (espada.getQuantidade() > 0) {
            qtdAtaques++;
            dwarf.perderVida();
            if (devePerderVida()) {
                perderVida();
            } else {
                ganharVida();
            }
        }
    }
}
>>>>>>> master
