import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test
{
    @Test
    public void testarAleatoriedade(){
        DadoD6 dado = new DadoD6();
        double n = 0;
        for(int i = 0; i<10000;i++){
            if (dado.sortear()==6)
                n+=1;
        }
        double prob = n/100;
        assertEquals(16.7, prob, 1.0);
    }
}
