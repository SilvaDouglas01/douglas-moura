import java.util.*;
public interface Estrategias
{
    List<Elfo> noturnosPorUltimo(List<Elfo> atacantes);
    List<Elfo> ataqueIntercalado(List<Elfo> atacantes);
}
