import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ItemTest
{
    @Test
    public void criarItemVerificarNome(){
        Item novoItem = new Item("Escudo", 1);
        assertEquals("Escudo", novoItem.getDescricao());
    }
    
    @Test
    public void criarItemVerificarQuantidade(){
        Item novoItem = new Item("Escudo", 25);
        assertEquals(25, novoItem.getQuantidade());
    }
}
