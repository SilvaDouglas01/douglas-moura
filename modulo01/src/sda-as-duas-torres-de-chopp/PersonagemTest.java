import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class PersonagemTest
{
    @Test
    public void adicionarItemInventarioElfo(){
        Personagem pers1 = new Elfo("Joaozinho");
        Item espada = new Item("Espada de aço", 1);
        pers1.ganharItem(espada);
        assertTrue(espada.equals(pers1.getInventario().obter(2)));
    }
    
    @Test
    public void adicionarItemInventarioDwarf(){
        Personagem pers1 = new Dwarf("Joaozinho");
        Item espada = new Item("Espada de aço", 1);
        pers1.ganharItem(espada);
        assertTrue(espada.equals(pers1.getInventario().obter(0)));
    }
    
    @Test
    public void removerItemInventarioDwarf(){
        Personagem pers1 = new Dwarf("Joaozinho");
        Item espada = new Item("Espada de aço", 1);
        pers1.ganharItem(espada);
        Item escudo = new Item("Escudo de madeira", 1);
        pers1.ganharItem(escudo);
        pers1.perderItem(espada);
        assertFalse(espada.equals(pers1.getInventario().obter(0)));
    }
    
    @Test
    public void removerItemInventarioElfo(){
        Personagem pers1 = new Elfo("Joaozinho");
        Item espada = new Item("Espada de aço", 1);
        pers1.ganharItem(espada);
        Item escudo = new Item("Escudo de madeira", 1);
        pers1.ganharItem(escudo);
        pers1.perderItem(espada);
        assertFalse(espada.equals(pers1.getInventario().obter(0)));
    }
    
    @Test 
    public void inventarioDwarf(){
        Personagem pers1 = new Dwarf("Joaozinho");
        Item espada = new Item("Espada de aço", 1);
        Inventario mochila = new Inventario();
        mochila.adicionar(espada);
        pers1.ganharItem(espada);
        assertEquals(mochila.obter(0), pers1.getInventario().obter(0));
    }
}
