import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
<<<<<<< HEAD
public class ExercitoDeElfosTest
{
    @Test
    public void alistarElfoVerde(){
        ElfoVerde elfinho = new ElfoVerde("Alfredo");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfinho);
        assertEquals(elfinho, exercito.getAlistados().get("Alfredo"));
    }
    
    @Test
    public void alistarElfoNoturno(){
        ElfoNoturno elfinho = new ElfoNoturno("Alfredo");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfinho);
        assertEquals(elfinho, exercito.getAlistados().get("Alfredo"));
    }
    
    @Test
    public void alistarElfoInvalido(){
        Elfo elfinho = new Elfo("Alfredo");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfinho);
        assertEquals(null, exercito.getAlistados().get("Alfredo"));
    }
    
    @Test
    public void obterElfosVivo(){
        ElfoNoturno elfinho = new ElfoNoturno("Alfredo");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfinho);
        HashMap<String, Elfo> esperado = new HashMap<>();
        esperado.put("Alfredo", elfinho);
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }
    
    @Test
    public void obterElfosMortos(){
        ElfoNoturno elfinho = new ElfoNoturno("Alfredo");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        exercito.alistarElfo(elfinho);
        HashMap<String, Elfo> esperado = new HashMap<>();
        esperado.put("Alfredo", elfinho);
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }
    
    @Test
    public void moverElfoParaMorto(){
        ElfoNoturno elfinho = new ElfoNoturno("Alfredo");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        HashMap<String, Elfo> esperado = new HashMap<>();
        esperado.put("Alfredo", elfinho);
        exercito.alistarElfo(elfinho);
        assertEquals(esperado, exercito.buscar(Status.VIVO));
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        elfinho.perderVida();
        esperado.put("Alfredo", elfinho);
        HashMap<String, Elfo> esperadoVazio = new HashMap<>();
        assertEquals(esperadoVazio, exercito.buscar(Status.VIVO));
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }
    
    // @Test
    // public void estregiaNoturnosPorUltimo(){
        // ExercitoDeElfos exercito = new ExercitoDeElfos();
        // List<Elfo> bagunca = new ArrayList<>();
        // List<Elfo> esperado = new ArrayList<>();
        // bagunca.add(new ElfoNoturno("Legolas").);
        // bagunca.add(new ElfoVerde("Will"));
        // bagunca.add(new ElfoNoturno("Gemlim"));
        // bagunca.add(new ElfoVerde("Alfred"));
        // bagunca.add(new ElfoNoturno("Frederic"));
        // bagunca.add(new ElfoVerde("Joao"));
        // bagunca.add(new ElfoNoturno("Douglas Nerd"));
        // bagunca.
    // }
}
=======

public class ExercitoDeElfosTest {

    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }

    @Test
    public void naoPodeAlistarElfo() {
        Elfo elfo = new Elfo("Common Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfo = new ElfoDaLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void buscarElfosVivosExistindo() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(vivo)
            );
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }

    @Test
    public void buscarElfosVivosNaoExistindo() {
        Elfo vivoTrocaPraMorto = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        vivoTrocaPraMorto.setStatus(Status.MORTO);
        exercito.alistar(vivoTrocaPraMorto);
        assertNull(exercito.buscar(Status.VIVO));
    }

    @Test
    public void buscarElfosMortosExistindo() {
        Elfo morto = new ElfoVerde("Undead Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        morto.setStatus(Status.MORTO);
        exercito.alistar(morto);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(morto)
            );
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }    

    @Test
    public void buscarElfosMortosNaoExistindo() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(vivo);
        assertNull(exercito.buscar(Status.MORTO));
    }
}

>>>>>>> master
