/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tiago
 */
public class ClienteTest {
    
    public ClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        EntityManager em = PersistenceUtils.getEm();
        if(!em.getTransaction().isActive())
            em.getTransaction().begin();
        em.createQuery("select c from Animal c", Animal.class).getResultList().forEach(em::remove);
        em.createQuery("select c from Cliente c", Cliente.class).getResultList().forEach(em::remove);
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Cliente.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Cliente instance = new Cliente();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setId method, of class Cliente.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        Cliente instance = new Cliente();
        instance.setId(id);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getNome method, of class Cliente.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Cliente instance = new Cliente();
        String expResult = null;
        String result = instance.getNome();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setNome method, of class Cliente.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "";
        Cliente instance = new Cliente();
        instance.setNome(nome);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getSexo method, of class Cliente.
     */
    @Test
    public void testGetSexo() {
        System.out.println("getSexo");
        Cliente instance = new Cliente();
        SexoType expResult = null;
        SexoType result = instance.getSexo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSexo method, of class Cliente.
     */
    @Test
    public void testSetSexo() {
        System.out.println("setSexo");
        SexoType sexo = null;
        Cliente instance = new Cliente();
        instance.setSexo(sexo);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getProfissao method, of class Cliente.
     */
    @Test
    public void testGetProfissao() {
        System.out.println("getProfissao");
        Cliente instance = new Cliente();
        String expResult = null;
        String result = instance.getProfissao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setProfissao method, of class Cliente.
     */
    @Test
    public void testSetProfissao() {
        System.out.println("setProfissao");
        String profissao = "";
        Cliente instance = new Cliente();
        instance.setProfissao(profissao);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    @Test
    public void testGetClienteById() {
        System.out.println("getClienteById");
        int id = 0;
        Cliente expResult = null;
        ClienteDAO clienteDao = new ClienteDAO();
        Cliente cliente = clienteDao.getClienteById(id);
        assertEquals(expResult, cliente);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of insereDezClientes method, of class Cliente.
     */
    @Test
    public void testInsereDezClientes() {
        System.out.println("insereDezClientes");
        ClienteDAO cliente = new ClienteDAO();
        cliente.insereDezClientes();
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of hashCode method, of class Cliente.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Cliente instance = new Cliente();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class Cliente.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Cliente instance = new Cliente();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class Cliente.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cliente instance = new Cliente();
        String expResult = "br.com.dbc.petshop.entity.Cliente[ id=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }
    
    @Test
    public void testBuscarClientePorNome() {
        System.out.println("buscarClientePorNome");
        ClienteDAO clienteDAO = new ClienteDAO();
        clienteDAO.insereDezClientes();
        String nome = "Clotilde1";
        ClienteDAO instance = ClienteDAO.getInstance();
        String expNome = "Clotilde1";
        List<Cliente> result = instance.buscarClientePorNome(nome);
        assertEquals(1, result.size());
        assertEquals(expNome, result.get(0).getNome());
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testBuscarClientePorNomeHibernate() {
        Cliente c = new Cliente("Clotilde", SexoType.M, "Laçador de Clóvis");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.save(c);
        t.commit();
        
        List<Cliente> clientes2 = new ArrayList<>();
        Session session2 = HibernateUtil.getSessionFactory().openSession();
        clientes2 = session2
                .createCriteria(Cliente.class)
                .add(Restrictions.eq("nome", "Clotilde").ignoreCase())
                .addOrder(Order.asc("id"))
                .list();
        assertEquals(1, clientes2.size());
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void somarValoresClientes(){
        ClienteDAO cliente = new ClienteDAO();
        cliente.insereDezClientes();
        EntityManager em = PersistenceUtils.getEm();
        List<Object> animais = em
                .createQuery("SELECT c.idCliente, SUM(c.valor) AS valor FROM Animal c GROUP BY ID_CLIENTE")
                //.setParameter("idCliente", 110)
                .getResultList();
        
        assertEquals(1, animais.size());
    }
    
}
