/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class AnimalTest {
    
    public AnimalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        EntityManager em = PersistenceUtils.getEm();
        if(!em.getTransaction().isActive())
            em.getTransaction().begin();
        em.createQuery("select c from Animal c", Animal.class).getResultList().forEach(em::remove);
        em.createQuery("select c from Cliente c", Cliente.class).getResultList().forEach(em::remove);
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class Animal.
     */
    
    @Test
    public void testInsereCincoAnimais() {
        System.out.println("insereCincoAnimais");
        AnimalDAO animalDao = new AnimalDAO();
        animalDao.inserirCincoAnimaisHibernate();
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
