/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import static br.com.dbc.petshop.entity.PersistenceUtils.getEm;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
/**
 *
 * @author eduardo.barbosa
 */
public class ClienteDAO {
    
    EntityManager em = PersistenceUtils.getEm();
    private static ClienteDAO instance;
    
    public static ClienteDAO getInstance(){
        return instance == null ? new ClienteDAO() : instance;
    }
     
    public Cliente getClienteById(final long id) {
        return em.find(Cliente.class, id);
     }
    
    
    public List<Cliente> buscarClientePorNome(String nome){
        return getEm()
                .createQuery("select c from Cliente c where c.nome like :nome", Cliente.class)
                .setParameter("nome", nome)
                .getResultList();
    }
    
    public void insereDezClientes() {
         this.em.getTransaction().begin();
         for(long j=1; j< 11; j++){
            Cliente c = new Cliente("Clotilde" + j, SexoType.M, "Laçador de Clóvis");
            this.em.persist(c);
            for(long i =1; i<11; i++){
                Animal a = new Animal("Clóvis " + i, SexoType.F, new BigDecimal(100*j));
                a.setIdCliente(c);
                this.em.persist(a);
            }
         }
         this.em.getTransaction().commit();
    }
}
