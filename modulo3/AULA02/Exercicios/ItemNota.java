/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pais.estado.cidade;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author douglas
 */
@Entity
@Table(name = "ITEM_NOTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemNota.findAll", query = "SELECT i FROM ItemNota i")
    , @NamedQuery(name = "ItemNota.findById", query = "SELECT i FROM ItemNota i WHERE i.id = :id")
    , @NamedQuery(name = "ItemNota.findByDescricao", query = "SELECT i FROM ItemNota i WHERE i.descricao = :descricao")})
public class ItemNota implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "DESCRICAO")
    private String descricao;
    @JoinColumn(name = "ID_PRODUTO", referencedColumnName = "ID")
    @ManyToOne
    private Produto idProduto;
    @ManyToOne
    private Nota idNota;

    public ItemNota() {
    }

    public ItemNota(BigDecimal id) {
        this.id = id;
    }

    public ItemNota(BigDecimal id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @XmlTransient
    public Nota getIdNota() {
        return idNota;
    }

    public void setNotaList(Nota idNota) {
        this.idNota = idNota;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemNota)) {
            return false;
        }
        ItemNota other = (ItemNota) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.pais.estado.cidade.ItemNota[ id=" + id + " ]";
    }
    
}
