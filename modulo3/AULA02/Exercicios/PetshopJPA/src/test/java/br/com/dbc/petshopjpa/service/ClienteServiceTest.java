/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.dao.PersistenceUtils;
import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.internal.util.reflection.Whitebox;

/**
 *
 * @author douglas
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        em.getTransaction().begin();
        em.createQuery("delete from Cliente").executeUpdate();
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    private final EntityManager em = PersistenceUtils.getEntityManager();
    /**
     * Test of getDAO method, of class ClienteService.
     */
    @Test
    public void testFindAll(){
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
                .nome("Joao")
                .animalList(Arrays.asList(Animal
                    .builder()
                    .nome("Pé de feijão")
                    .build()))
                .build();
        em.persist(c);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> clientes = instance.findAll();
        
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getId(),
                clientes.stream().findFirst().get().getId());
        Assert.assertEquals(c.getAnimalList().size(),
                clientes.stream().findFirst().get().getAnimalList().size());
        Assert.assertEquals(c.getId(),
                clientes.stream().findFirst().get().getId());
        
        
    }
    
    @Test
    public void testFindOneMocked(){
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente created = Cliente.builder()
                .id(1l)
                .nome("Jovem")
                .animalList(Arrays
                        .asList(Animal.builder()
                                .id(1l)
                                .nome("Velho")
                                .build()))
                .build();
        Mockito.doReturn(created).when(clienteDAO).findOne(created.getId());
        Cliente returned = clienteService.findOne(created.getId());
        Assert.assertEquals(created.getId(), returned.getId());
        Mockito.verify(clienteDAO, times(1)).findOne(created.getId());
    }
    
    @Test
    public void testDeleteMocked() throws NotFoundException{
        Cliente cliente1 = Cliente.builder()
                .id(new Long(1))
                .nome("Cliente 1")
                .animalList(
                    Arrays.asList(Animal.builder()
                        .nome("Animal 1")
                        .build())).build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(cliente1.getId());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Mockito.when(clienteService.findOne(cliente1.getId())).thenReturn(cliente1);
        clienteService.delete(cliente1.getId());
        Mockito.verify(daoMock, times(1)).delete(cliente1.getId());
    }
    
}
