/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.dao;

import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.Arrays;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author douglas
 */
public class ClienteDAOTest {
    
    public ClienteDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        em.getTransaction().begin();
        em.createQuery("delete from Cliente").executeUpdate();
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    private final EntityManager em = PersistenceUtils.getEntityManager();

    /**
     * Test of insereDezClientes method, of class ClienteDAO.
     */
    @Test
    public void testInsereDezClientes() {
        System.out.println("insereDezClientes");
        ClienteDAO instance = new ClienteDAO();
        instance.insereDezClientes();
        assertEquals(10, instance.findAll().size());
        assertEquals(10, instance.findAll().get(0).getAnimalList().size());
    }
    
    @Test 
    public void testGetSomaValores(){
        ClienteDAO instance = new ClienteDAO();
        instance.insereDezClientes();
        assertEquals(20000, instance.getSomaValores(instance.findAll().get(0).getId()), 0.0);
    }
}
