/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.AbstractDAO;
import java.util.List;

/**
 *
 * @author douglas
 */
public abstract class AbstractCrudService<E, I, DAO extends AbstractDAO<E, I>> {
    
    public abstract DAO getDAO();
    
    public List<E> findAll(){
        return getDAO().findAll();
    }
    
    public E findOne(I id){
        return getDAO().findOne(id);
    }
    
    public void create(E entity){
        getDAO().create(entity);
    }
    
    public void update(E entity){
        getDAO().update(entity);
    }
    
    public void delete(I id){
        getDAO().delete(id);
    }
}
