/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.dao;

import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author douglas
 */
public class ClienteDAO extends AbstractDAO<Cliente, Long>{

    @Override
    protected Class getEntityClass(){
        return Cliente.class;
    }
    
    public void insereDezClientes() {
        EntityManager em = PersistenceUtils.getEntityManager();
         em.getTransaction().begin();
         for(long j=1; j< 11; j++){
            Cliente c = Cliente.builder()
                .nome("Cliente "+j).build();
            List<Animal> animais = new ArrayList<>();
            for(long i =1; i<11; i++){
                Animal a = Animal.builder()
                    .nome("Cliente "+j)
                    .valor(2000)
                    .build();
                c.setAnimalList(Arrays.asList(a));
                animais.add(a);
                em.persist(a);
            }
            c.setAnimalList(animais);
            em.persist(c);
         }
         em.getTransaction().commit();
    }
    
    public double getSomaValores(Long id){
        EntityManager em = PersistenceUtils.getEntityManager();
        Cliente cliente = findOne(id);
                //.createQuery("SELECT c.clienteList, SUM(c.valor) AS valor FROM Animal c GROUP BY ID_CLIENTE")
                //.createQuery("select c from Cliente c where c.id like :id", Cliente.class)
                //.setParameter("id", id)
                //.getSingleResult();
        double valores = new Long(0);
        for(int i = 0; i<cliente.getAnimalList().size();i++){
            valores= valores + cliente.getAnimalList().get(i).getValor();
        }
        
        return valores;
    }
}
