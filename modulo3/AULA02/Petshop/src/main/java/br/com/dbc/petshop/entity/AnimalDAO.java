/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import java.math.BigDecimal;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author douglas
 */
public class AnimalDAO {
    
    EntityManager em = PersistenceUtils.getEm();
    private static AnimalDAO instance;
    
    public static AnimalDAO getInstance(){
        return instance == null ? new AnimalDAO() : instance;
    }
    
    public void inserirCincoAnimaisHibernate(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
        for(int i=0; i<5; i++){
            Animal a = new Animal("Clóvis " + i, SexoType.F, new BigDecimal(100*i));
            session.save(a);
        }
        t.commit();
    }
}
