/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.projetocomapachecommonsutil;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author douglas
 */
public class Main {
    
    public static void main(String[] args){
        System.out.println(StringUtils.isEmpty(""));
        System.out.println(StringUtils.isEmpty(" "));
        System.out.println(StringUtils.isBlank(" "));
        System.out.println(StringUtils.isBlank(""));
        System.out.println(StringUtils.leftPad("pudim", 10));
        System.out.println(StringUtils.rightPad("pudim", 10));
        System.out.println(StringUtils.trimToNull("           pudim de pao        "));
        System.out.println(StringUtils.equals("pudim de pao", "pudim de leite"));
        System.out.println(StringUtils.equals("pudim de pao", "pudim de pao"));
        System.out.println(StringUtils.contains("pudim de pao", "pudim"));
    }
    
}
