/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.pkg6;

import java.util.Scanner;

/**
 *
 * @author douglas
 */
public class Lista6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner input = new Scanner(System.in);
        int nFuncionarios;
        double salarioTotal = 0;
        double salarioDepto2 = 0;
        int nFuncionariosDepto2 = 0;
        Funcionario maiorSalario = new Funcionario("Clóvis", 1, "RH", 0.0);

        do {
            System.out.println("Digite o número de funcionarios: ");
            nFuncionarios = input.nextInt();
            if (nFuncionarios < 0) {
                System.out.print("Número invalido: ");
            }
        } while (nFuncionarios < 0);
        int randomNum = 0 + (int) (Math.random() * (3 - 0));

        for (int i = 0; i < nFuncionarios; i++) {
            System.out.println("Digite o nome do funcionário");
            String nome = input.next();
            System.out.println("Digite o número do departamento");
            int numeroDepto = input.nextInt();
            System.out.println("Digite o nome do departamento");
            String dep = input.next();
            System.out.println("Digite o sálario do funcionario");
            double salario = input.nextDouble();

            boolean flag = randomNum == 1 ? true : false;

            Funcionario func;

            if (flag) {
                func = new Funcionario(nome, numeroDepto, dep, salario);
            } else {
                System.out.println("Digite o nome do secretário: ");
                String secretario = input.next();
                func = new Gerente(nome, new Departamento(numeroDepto, nome), salario, secretario);
            }

            int reajuste;

            do {
                System.out.println("Digite o codigo de reajuste: ");
                reajuste = input.nextInt();
                if (reajuste < 0 && reajuste > 3) {
                    System.out.print("Reajuste invalido: ");
                }
            } while (reajuste < 0 && reajuste > 3);

            func.reajustaSalario(reajuste);
            salarioTotal += func.getSalario();
            if (func.getSalario() > maiorSalario.getSalario()) {
                maiorSalario = func;
            }
            if (numeroDepto == 2) {
                salarioDepto2 += func.getSalario();
                nFuncionariosDepto2 += 1;
            }

        }
        Gerente gerente;
        System.out.println("Valor da folha de pagamento: " + salarioTotal);
        System.out.println("========================");
        System.out.println("Funcionario com maior salario");
        System.out.println("Nome: " + maiorSalario.getNome());
        System.out.println("Salario: " + maiorSalario.getSalario());
        System.out.println("Departamento: " + maiorSalario.getDepartamento().getNome());
        if (maiorSalario instanceof Gerente) {
            gerente = (Gerente) maiorSalario;
            System.out.println("Secretario: "+ gerente.getNomeSecretario());
        }
        System.out.println("========================");
        System.out.println("Salario médio do departamento 2: " + salarioDepto2 / nFuncionariosDepto2);

    }

}
