/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.pkg6;

/**
 *
 * @author douglas
 */
public class Funcionario {
// instance variables - replace the example below with your own

    private String nome;
    private Departamento depto;
    private double salario;

    public Funcionario(String nome, Departamento depto, double salario) {
        this.nome = nome;
        this.depto = depto;
        this.salario = salario;
    }

    public Funcionario(String nome, int numeroDepartamento, String nomeDepartamento, double salario) {
        depto = new Departamento(numeroDepartamento, nomeDepartamento);
        this.nome = nome;
        this.depto = depto;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public double getSalario() {
        return salario;
    }

    public Departamento getDepartamento() {
        return depto;
    }

    public void reajustaSalario(int cod) {//Não cosidera o depto
        salario = cod == 1 ? salario + (salario * 0.20) : cod == 3 ? salario + (salario * 0.13) : salario + (salario * 0.05);
    }

    public void reajustaSalario(int cod, double limite) {
        this.reajustaSalario(cod);
        if (depto.getNumero() == 25 && salario<limite)
            salario += 57;
    }
}

