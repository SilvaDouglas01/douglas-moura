/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.pkg6;

/**
 *
 * @author douglas
 */
public class Departamento {

//------ atributos------
    private int numero;
    private String nome;

//-------- construtor-----------------------------------
    public Departamento(int num, String nom) {
        setNumero(num);
        setNome(nom);
    }// fim do construtor

//--------------métodos----------------------
    public void setNumero(int n) {
        if (n > 0) {
            numero = n;
        }
    }//--fim do método setNumero ---------------

    public void setNome(String n) {
        if (n.length() > 0) {
            nome = n;
        }
    }//---fim do método setNome ------------------

    public int getNumero() {
        return numero;
    }//---fim do método getNumero -----------------

    public String getNome() {
        return nome;
    }//---fim do método getNome -------------------

}//fim da classe

