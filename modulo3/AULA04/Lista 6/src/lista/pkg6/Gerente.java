/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.pkg6;

/**
 *
 * @author douglas
 */
public class Gerente extends Funcionario {
    
    private String nomeSecretario;

    public String getNomeSecretario() {
        return nomeSecretario;
    }

    public void setNomeSecretario(String nomeSecretario) {
        this.nomeSecretario = nomeSecretario;
    }
    
    public Gerente(String nome, Departamento depto, double salario) {
        super(nome, depto, salario);
        this.setNomeSecretario("Cargo Vago");
    }
    
    public Gerente(String nome, Departamento depto, double salario, String secretario) {
        super(nome, depto, salario);
        this.setNomeSecretario(secretario);
    }
    
}
