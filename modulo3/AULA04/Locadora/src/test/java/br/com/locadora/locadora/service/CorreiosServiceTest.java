/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.dto.EnderecoDTO;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author douglas
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CorreiosServiceTest{
    
    @Autowired
    private CorreiosService correiosService;
    
    public CorreiosServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConsultaCEP() {
        EnderecoDTO dto = correiosService.consultaCEP("93222613");
        
        Assert.assertEquals("Vargas", dto.getBairro());
        Assert.assertEquals("Sapucaia do Sul", dto.getCidade());
        Assert.assertEquals("RS", dto.getEstado());
        Assert.assertTrue(dto.getRua().equalsIgnoreCase("Rua Nossa Senhora dos Navegantes"));
    }

    
}
