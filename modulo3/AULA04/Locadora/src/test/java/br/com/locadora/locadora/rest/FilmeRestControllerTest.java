/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.LocadoraApplicationTests;
import br.com.locadora.locadora.dto.AluguelDTO;
import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.dto.FilmeDTO;
import br.com.locadora.locadora.dto.MidiaDTO;
import br.com.locadora.locadora.entity.Cliente;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.repository.ClienteRepository;
import br.com.locadora.locadora.repository.FilmeRepository;
import br.com.locadora.locadora.repository.MidiaRepository;
import br.com.locadora.locadora.repository.ValorMidiaRepository;
import br.com.locadora.locadora.service.AluguelService;
import br.com.locadora.locadora.service.ClienteService;
import br.com.locadora.locadora.service.FilmeService;
import br.com.locadora.locadora.service.MidiaService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author douglas
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private MidiaService midiaService;
 
    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private ClienteService clienteService;

    protected AbstractRestController getController() {
        return filmeRestController;
    }
    
    @Before
    public void setUp() {
        this.restMockMvc = MockMvcBuilders.standaloneSetup(getController())
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();;
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

   
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCreateTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        //.andDo(h->{System.out.println(h.getResponse().getContentAsString());});
//.andExpect(MockMvcResultMatchers.jsonPath("$.midiaList.[*]").isNotEmpty());
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f.getLancamento(), filmes.get(0).getLancamento());
    }

    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateQuantidadeMidiasIguaisTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        List<MidiaDTO> midias2 = new ArrayList<>();

        MidiaDTO m2_1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(3.0)
                .build();
        midias2.add(m2_1);
        MidiaDTO m2_2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_2);
        MidiaDTO m2_3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_3);

        FilmeDTO f2 = FilmeDTO.builder()
                .id(f.getId())
                .titulo("João e o Feijaojao")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias2)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", f2.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f2.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f2.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f2.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f2.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(2, midiaService.countByTipo(MidiaType.VHS).getCount());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.DVD).getCount());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.BLUE_RAY).getCount());

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateSemUmTipoDeMidiaInicialmenteTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        List<MidiaDTO> midias2 = new ArrayList<>();

        MidiaDTO m2_1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(3.0)
                .build();
        midias2.add(m2_1);
        MidiaDTO m2_2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_2);
        MidiaDTO m2_3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_3);

        FilmeDTO f2 = FilmeDTO.builder()
                .id(f.getId())
                .titulo("João e o Feijaojao")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias2)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", f2.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f2.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f2.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f2.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f2.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(2, midiaService.countByTipo(MidiaType.VHS).getCount());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.DVD).getCount());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.BLUE_RAY).getCount());

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateBadRequestIdNullTest() throws Exception {

        FilmeDTO f2 = FilmeDTO.builder()
                .id(null)
                .titulo("João e o Feijaojao")
                .categoria(Categoria.AVENTURA)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/1/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f2)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateBadRequestIdsDiferentesTest() throws Exception {

        FilmeDTO f2 = FilmeDTO.builder()
                .id(2l)
                .titulo("João e o Feijaojao")
                .categoria(Categoria.AVENTURA)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/1/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f2)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateQuantidadeMidiasMaiorTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        List<MidiaDTO> midias2 = new ArrayList<>();

        MidiaDTO m2_1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(3)
                .valor(3.0)
                .build();
        midias2.add(m2_1);
        MidiaDTO m2_2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(3)
                .valor(3.0)
                .build();
        midias2.add(m2_2);
        MidiaDTO m2_3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(3)
                .valor(3.0)
                .build();
        midias2.add(m2_3);

        FilmeDTO f2 = FilmeDTO.builder()
                .id(f.getId())
                .titulo("João e o Feijaojao")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias2)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", f2.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f2.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f2.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f2.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f2.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(3, midiaService.countByTipo(MidiaType.VHS).getCount());
        Assert.assertEquals(3, midiaService.countByTipo(MidiaType.DVD).getCount());
        Assert.assertEquals(3, midiaService.countByTipo(MidiaType.BLUE_RAY).getCount());

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateQuantidadeMidiasAbaixoTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(3)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(2)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        List<MidiaDTO> midias2 = new ArrayList<>();

        MidiaDTO m2_1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_1);
        MidiaDTO m2_2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_2);
        MidiaDTO m2_3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.0)
                .build();
        midias2.add(m2_3);

        FilmeDTO f2 = FilmeDTO.builder()
                .id(f.getId())
                .titulo("João e o Feijaojao")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias2)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", f2.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f2.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f2.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f2.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f2.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.VHS).getCount());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.DVD).getCount());
        Assert.assertEquals(1, midiaService.countByTipo(MidiaType.BLUE_RAY).getCount());

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void precosTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        Filme filme = filmeRepository.findAll().get(0);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/precos/{id}", filme.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(4))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].tipo").value("VHS"));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeSearchTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search?page=0&size=10&sort=id,desc&categoria=AVENTURA&titulo=ddssd&lancamentoIni=10/10/2018&lancamentoFim=11/10/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()));

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeSearchSemDataTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search?page=0&size=10&sort=id,desc&categoria=AVENTURA&titulo=joão"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()));

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void countByTipoTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Filme filme = filmeRepository.findAll().get(0);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/count/{id}/DVD", filme.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.count").value("1"));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoComTudoDisponivelTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("oão")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[1].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[2].disponivel").value(true));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoSemUmTipoDeMidiaTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("oão")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.size()").value(2));
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoComMaisDeUmaMidiaAlugadaPorDiferentesAlugueisTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("oão")
                .build();
        
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();
        Midia midia2 = midiaRepository.findAll().get(1);
        List<Long> midias2 = new ArrayList<>();
        midias2.add(midia2.getId());
        AluguelDTO aluguel2 = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias2)
                .build();

        aluguelService.retirar(aluguel);
        aluguelService.retirar(aluguel2);

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].disponivel").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[1].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[2].disponivel").value(true));
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoComMidiasIndisponiveisTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(1)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("oão")
                .build();
        
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);
        
        LocalDate previsao = LocalDate.now().plusDays(1l);
        

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].disponivel").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[1].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[2].disponivel").value(true));
    }
    
    @Test
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void filmeCatalogoComTudoDisponivelStandardUserTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        FilmeDTO filme = FilmeDTO.builder()
                .titulo("oão")
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[1].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[2].disponivel").value(true));
    }
    
    @Test
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void filmeSearchStandardTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search?page=0&size=10&sort=id,desc&categoria=AVENTURA&titulo=ddssd&lancamentoIni=10/10/2018&lancamentoFim=11/10/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()));

    }
}
