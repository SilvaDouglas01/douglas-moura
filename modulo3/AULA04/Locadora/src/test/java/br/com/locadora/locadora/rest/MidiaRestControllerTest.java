/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.LocadoraApplicationTests;
import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.dto.FilmeDTO;
import br.com.locadora.locadora.dto.MidiaDTO;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.repository.FilmeRepository;
import br.com.locadora.locadora.repository.MidiaRepository;
import br.com.locadora.locadora.repository.ValorMidiaRepository;
import br.com.locadora.locadora.service.FilmeService;
import br.com.locadora.locadora.service.MidiaService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas
 */
public class MidiaRestControllerTest extends LocadoraApplicationTests {
    
    
    @Autowired
    private MidiaRestController midiaRestController;
    
    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private MidiaService midiaService;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void BeforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();;
        filmeRepository.deleteAll();
    }
    
    @After
    public void tearDown() {
    }



    @Override
    protected AbstractRestController getController() {
        return midiaRestController;
    }
    
    @Test
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void midiaSearchTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        
        filmeService.saveDTO(f);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/search?page=0&size=10&sort=id,desc&categoria=AVENTURA&titulo=Joao&lancamentoIni=10/10/2018&lancamentoFim=11/10/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(4));

    }
    
    @Test
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void midiaSearchSemDataTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();
        
        filmeService.saveDTO(f);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/search?page=0&size=10&sort=id,desc&categoria=AVENTURA&titulo=João"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(4));

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void countByTipoTest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/count/DVD")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.count").value("1"));
    }
}
