/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author douglas
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AluguelServiceTest {
    
    @Autowired
    private LocalDateService localDateService;
    
    public AluguelServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testNow(){
        Assert.assertEquals(localDateService.now(), LocalDate.now());
        Assert.assertEquals(localDateService.ZonedNow().getHour(), ZonedDateTime.now().getHour());
    }
    
}
