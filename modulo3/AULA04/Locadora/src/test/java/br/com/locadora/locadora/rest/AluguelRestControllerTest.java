/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.LocadoraApplicationTests;
import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Cliente;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.dto.AluguelDTO;
import br.com.locadora.locadora.dto.FilmeDTO;
import br.com.locadora.locadora.dto.MidiaDTO;
import br.com.locadora.locadora.repository.AluguelRepository;
import br.com.locadora.locadora.repository.ClienteRepository;
import br.com.locadora.locadora.repository.FilmeRepository;
import br.com.locadora.locadora.repository.MidiaRepository;
import br.com.locadora.locadora.repository.ValorMidiaRepository;
import br.com.locadora.locadora.service.AluguelService;
import br.com.locadora.locadora.service.ClienteService;
import br.com.locadora.locadora.service.FilmeService;
import br.com.locadora.locadora.service.LocalDateService;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas
 */
public class AluguelRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private AluguelRestController aluguelRestController;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    public FilmeService filmeService;

    @MockBean
    public LocalDateService localDateService;

    @Autowired
    public ClienteService clienteService;

    @Autowired
    public AluguelService aluguelService;

    @Override
    protected AbstractRestController getController() {
        return aluguelRestController;
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testRetirada() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        LocalDate previsao = LocalDate.now().plusDays(3);

        Double precoEsperado = 1.9 + 1.9 + 2.9;

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco").value(precoEsperado));

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testRetiradaBadRequest() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(null)
                .midias(midias_)
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucao() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);

        LocalDate previsao = LocalDate.now().plusDays(3);

        Double precoEsperado = 1.9 + 1.9 + 2.9;

        AluguelDTO aluguel2 = AluguelDTO.builder()
                .midias(midias_)
                .build();

        Mockito.when(localDateService.ZonedNow()).thenReturn(ZonedDateTime.now().withHour(10));

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.devolucao").value(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucaoSemDevolverTodos() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);

        midias_.remove(2);
        LocalDate previsao = LocalDate.now().plusDays(3);

        Double precoEsperado = 1.9 + 1.9 + 2.9;

        AluguelDTO aluguel2 = AluguelDTO.builder()
                .midias(midias_)
                .build();

        Mockito.when(localDateService.ZonedNow()).thenReturn(ZonedDateTime.now().withHour(10));

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucaoComMulta() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);

        LocalDate previsao = LocalDate.now().plusDays(3);

        Double precoEsperado = 1.9 + 1.9 + 2.9;

        AluguelDTO aluguel2 = AluguelDTO.builder()
                .midias(midias_)
                .build();

        Mockito.when(localDateService.ZonedNow()).thenReturn(ZonedDateTime.now().plusDays(4l));

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.devolucao").value(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucaoNoDiaDaPrevisaoApos16Horas() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);

        LocalDate previsao = LocalDate.now().plusDays(3);

        Double precoEsperado = 1.9 + 1.9 + 2.9;

        AluguelDTO aluguel2 = AluguelDTO.builder()
                .midias(midias_)
                .build();

        Mockito.when(localDateService.ZonedNow()).thenReturn(ZonedDateTime.now().plusDays(3l).withHour(18));

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.devolucao").value(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))));
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucaoNoDiaDaPrevisaoAntesDe16Horas() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);

        LocalDate previsao = LocalDate.now().plusDays(3);

        Double precoEsperado = 1.9 + 1.9 + 2.9;

        AluguelDTO aluguel2 = AluguelDTO.builder()
                .midias(midias_)
                .build();

        Mockito.when(localDateService.ZonedNow()).thenReturn(ZonedDateTime.now().plusDays(3l).withHour(15));

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(previsao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco").value(precoEsperado))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.devolucao").value(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testDevolucaoHoje() throws Exception {
        List<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO m1 = MidiaDTO.builder()
                .tipo(MidiaType.VHS)
                .quantidade(2)
                .valor(1.9)
                .build();
        midias.add(m1);
        MidiaDTO m2 = MidiaDTO.builder()
                .tipo(MidiaType.DVD)
                .quantidade(1)
                .valor(2.9)
                .build();
        midias.add(m2);
        MidiaDTO m3 = MidiaDTO.builder()
                .tipo(MidiaType.BLUE_RAY)
                .quantidade(1)
                .valor(3.9)
                .build();
        midias.add(m3);

        LocalDate date = LocalDate.of(2018, 10, 10);

        FilmeDTO f = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(date)
                .categoria(Categoria.AVENTURA)
                .midia(midias)
                .build();

        filmeService.saveDTO(f);
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteService.save(c1);
        Cliente cliente = clienteRepository.findAll().get(0);
        Midia midia1 = midiaRepository.findAll().get(0);
        Midia midia2 = midiaRepository.findAll().get(1);
        Midia midia3 = midiaRepository.findAll().get(2);
        List<Long> midias_ = new ArrayList<>();
        midias_.add(midia1.getId());
        midias_.add(midia2.getId());
        midias_.add(midia3.getId());
        AluguelDTO aluguel = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(midias_)
                .build();

        aluguelService.retirar(aluguel);

        AluguelDTO aluguel2 = AluguelDTO.builder()
                .midias(midias_)
                .build();

        Mockito.when(localDateService.now()).thenReturn(LocalDate.now().plusDays(3l));

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguel2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(3));
    }
}
