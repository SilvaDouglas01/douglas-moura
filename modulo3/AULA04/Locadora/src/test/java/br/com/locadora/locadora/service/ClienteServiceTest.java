/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.entity.Cliente;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;

/**
 *
 * @author douglas
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRepository method, of class ClienteService.
     */
    
    public void testFindbyId() {
        System.out.println("getRepository");
        ClienteService instance = new ClienteService();
        Cliente cliente = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        Mockito.doReturn(cliente).when(instance).findById(cliente.getId());
        Object returned = instance.findById(cliente.getId());
        //Assert.assertEquals(cliente.getId(), returned.getId());
        Mockito.verify(instance, times(1)).findById(cliente.getId());
    }
    
}
