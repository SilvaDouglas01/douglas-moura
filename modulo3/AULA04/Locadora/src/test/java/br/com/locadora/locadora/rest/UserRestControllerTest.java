/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.dto.UserDTO;
import br.com.locadora.locadora.repository.UserRepository;
import br.com.locadora.locadora.service.AppUserDetailsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author douglas
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRestControllerTest {

    protected MockMvc restMockMvc;

    @Autowired
    protected MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AppUserDetailsService appUserDetailsService;

    @Autowired
    private UserRestController userRestController;

    public UserRestControllerTest() {
    }

    protected UserRestController getController() {
        return userRestController;
    }

    @Before
    public void setUp() {

        this.restMockMvc = MockMvcBuilders.standaloneSetup(getController())
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void beforeTest() {
        userRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testUserCreate() throws Exception {

        UserDTO user = UserDTO.builder()
                .firstName("Clovis")
                .lastName("da Silva")
                .username("SIlvaClovis")
                .password("coxinha123")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(user.getUsername()));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testGetUsers() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/user"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "john.doe",
            password = "jwtpass",
            authorities = {"STANDARD_USER"})
    public void testGetUsersAccessDenied() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/user"));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testChangePassword() throws Exception {

        UserDTO user = UserDTO.builder()
                .firstName("Clovis")
                .lastName("da Silva")
                .username("SIlvaClovis0101")
                .password("coxinha123")
                .build();

        getController().create(user);

        UserDTO user2 = UserDTO.builder()
                .username("SIlvaClovis0101")
                .password("coxinha123")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(user.getUsername()));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testLoadUserByUsername() {
        UserDTO user = UserDTO.builder()
                .firstName("Clovis")
                .lastName("da Silva")
                .username("SIlvaClovis")
                .password("coxinha123")
                .build();
        appUserDetailsService.create(user);
        UserDetails user_ = appUserDetailsService.loadUserByUsername(user.getUsername());
        Assert.assertEquals(user_.getUsername(), user.getUsername());
    }
    
    @Test(expected = UsernameNotFoundException.class)
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testLoadUserByUsernameNotFound() {
        UserDetails user_ = appUserDetailsService.loadUserByUsername("pudim");
    }
}
