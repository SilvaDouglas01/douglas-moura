/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.LocadoraApplicationTests;
import br.com.locadora.locadora.dto.EnderecoDTO;
import br.com.locadora.locadora.entity.Cliente;
import br.com.locadora.locadora.repository.ClienteRepository;
import br.com.locadora.locadora.service.ClienteService;
import br.com.locadora.locadora.service.CorreiosService;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author douglas
 */
public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;
    
    @MockBean
    private CorreiosService correiosService;
    
    @Autowired
    private ClienteService clienteService;
    

    @Override
    protected AbstractRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteCreateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteCreateBadRequestTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .id(1l)
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteDeleteTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.save(c);
        Long clienteId = clienteRepository.findAll().get(0).getId();
        
        restMockMvc.perform(MockMvcRequestBuilders.delete("/api/cliente/{id}", clienteId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(0, clientes.size());
        
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindAllTest() throws Exception {
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        Cliente c2 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.saveAndFlush(c1);
        clienteRepository.saveAndFlush(c2);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].nome").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(2));
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindByIdTest() throws Exception {
        Cliente c1 = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.saveAndFlush(c1);
        
        Cliente cliente = clienteRepository.findAll().get(0);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/{id}", c1.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(cliente.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value("nome"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value("Vargas"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value("9999"));
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.save(c);
        Cliente cliente = clienteRepository.findAll().get(0);
        Cliente c1 = Cliente.builder()
                .id(cliente.getId())
                .nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/{id}", c1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c1.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c1.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c1.getBairro()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c1.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c1.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals(c1.getTelefone(), clientes.get(0).getTelefone());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateBadRequestIdNullTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.save(c);
        Cliente cliente = clienteRepository.findAll().get(0);
        Cliente c1 = Cliente.builder()
                .id(null)
                .nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/{id}", cliente.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c1)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateBadRequestIdsNullTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.save(c);
        Cliente c1 = Cliente.builder()
                .id(null)
                .nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/null/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c1)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateBadRequestIdsDiferentesTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.save(c);
        Cliente c1 = Cliente.builder()
                .id(2l)
                .nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/null/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c1)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateBadRequestIdUrlNullTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();
        clienteRepository.save(c);
        Cliente c1 = Cliente.builder()
                .id(2l)
                .nome("nome").bairro("Vargas")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .rua("Rua Nossa Senhora dos Navegantes")
                .telefone("9999")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/3/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c1)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void consultaCepMockedTest() throws Exception {
        
        EnderecoDTO dto = EnderecoDTO.builder()
                .bairro("Ipiranga")
                .rua("Rua Marieta Francisca Souza Silva")
                .cidade("Sapucaia do Sul")
                .estado("RS")
                .build();
        
        Mockito.when(correiosService.consultaCEP(ArgumentMatchers.anyString())).thenReturn(dto);
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/cep/{cep}", "93230483"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value("Ipiranga"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value("Rua Marieta Francisca Souza Silva"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value("Sapucaia do Sul"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value("RS"));
    }
}
