/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.repository;

import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Filme;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas
 */
public interface FilmeRepository extends JpaRepository<Filme, Long>{
    
    Page<Filme> findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(String titulo, Categoria categoria, LocalDate ini, LocalDate fim, Pageable pageable);
    
    List<Filme> findByTituloIgnoreCaseContaining(String titulo);
}
