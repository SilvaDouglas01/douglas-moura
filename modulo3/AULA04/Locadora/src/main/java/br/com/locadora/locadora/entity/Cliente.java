/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CLIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
    , @NamedQuery(name = "Cliente.findById", query = "SELECT c FROM Cliente c WHERE c.id = :id")
    , @NamedQuery(name = "Cliente.findByNome", query = "SELECT c FROM Cliente c WHERE c.nome = :nome")
    , @NamedQuery(name = "Cliente.findByTelefone", query = "SELECT c FROM Cliente c WHERE c.telefone = :telefone")})
public class Cliente extends AbstractEntity implements Serializable {

    @Size(min = 1, max = 100)
    @NotNull
    @Column(name = "NOME")
    private String nome;
    @Size(min = 1, max = 10)
    @Column(name = "TELEFONE")
    private String telefone;
    @Size(min = 1, max = 100)
    @Column(name = "RUA")
    @Size(min = 1, max = 100)
    @NotNull
    private String rua;
    @Column(name = "BAIRRO")
    @Size(min = 1, max = 100)
    @NotNull
    private String bairro;
    @Column(name = "CIDADE")
    @Size(min = 1, max = 100)
    @NotNull
    private String cidade;
    @Column(name = "ESTADO")
    @Size(min = 1, max = 100)
    @NotNull
    private String estado;
    @Column(name = "NUMERO")
    @Size(min = 1, max = 100)
    private String numero;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
}
