/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.repository;

import br.com.locadora.locadora.entity.Aluguel;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author douglas
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {

    @Query("select al from Aluguel al where al.previsao = :hoje ")
    List<Aluguel> findByPrevisao(@Param("hoje") LocalDate hoje);

}
