/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.repository;

import br.com.locadora.locadora.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    
}
