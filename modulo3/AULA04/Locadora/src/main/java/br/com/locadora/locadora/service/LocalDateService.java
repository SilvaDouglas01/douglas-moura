/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.springframework.stereotype.Service;

/**
 *
 * @author douglas
 */
@Service
public class LocalDateService {
    
    
    public LocalDate now(){
        return LocalDate.now();
    }
    
    public ZonedDateTime ZonedNow(){
        return ZonedDateTime.now();
    }
}
