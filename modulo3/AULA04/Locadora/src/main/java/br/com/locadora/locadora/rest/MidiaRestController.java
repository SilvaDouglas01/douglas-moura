/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author douglas
 */
@RestController
@RequestMapping("/api/midia")
public class MidiaRestController extends AbstractRestController<Midia>{

    @Autowired
    private MidiaService service;
   

    @Override
    public MidiaService getService() {
        return service;
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @GetMapping("/count/{tipo}")
    public ResponseEntity<?> countByTipo(@PathVariable MidiaType tipo){
        return ResponseEntity.ok(getService().countByTipo(tipo));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER') OR hasAuthority('STANDARD_USER')")
    @GetMapping("/search")
    public ResponseEntity<?> search(Pageable pageable,
            @RequestParam(name = "titulo", required = false) String titulo,
            @RequestParam(name = "categoria", required = false) Categoria categoria,
            @RequestParam(name = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(name = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoFim) {
        return ResponseEntity.ok(getService().search(titulo, categoria, lancamentoIni, lancamentoFim, pageable));
    }
}
