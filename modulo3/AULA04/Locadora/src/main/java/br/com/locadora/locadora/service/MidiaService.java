/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.dto.CountDTO;
import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.dto.MidiaDTO;
import br.com.locadora.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas
 */
@Service
public class MidiaService extends AbstractCrudService{

    @Autowired
    private MidiaRepository repository;
    
    
    @Override
    public MidiaRepository getRepository() {
        return repository;
    }
    
    public Page<Midia> search(String titulo, Categoria categoria, LocalDate ini, LocalDate fim, Pageable pageable) {
        if (ini == null) {
            ini = LocalDate.MIN;
        }
        if (fim == null) {
            fim = LocalDate.MIN;
        }
        
        return getRepository().findByIdFilmeTituloContainingIgnoreCaseOrIdFilmeCategoriaOrIdFilmeLancamentoBetween(titulo, categoria, ini, fim, pageable);
    }
    
    public CountDTO countByTipo(MidiaType tipo){
        Long midias = getRepository().countByMidiaType(tipo);
        /*Long count = 0l;
        for (Midia midia : midias) {
             if(midia.getMidiaType().equals(tipo))
                 count++;
        }*/
        return CountDTO.builder()
                .count(midias)
                .build();
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Midia saveMidiaDTO(@NotNull @Valid MidiaDTO midiaDTO, Filme filme) {
        Midia midia = Midia.builder()
            .midiaType(midiaDTO.getTipo())
            .idFilme(filme)
            .build();
        return getRepository().save(midia);
    }
    
}
