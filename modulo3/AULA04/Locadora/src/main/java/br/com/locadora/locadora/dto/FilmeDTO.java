/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.dto;

import br.com.locadora.locadora.entity.Categoria;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author douglas
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilmeDTO {
    
    private String titulo;
    
    private Long id;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDate lancamento;
    
    private Categoria categoria;
    
    private List<MidiaDTO> midia;
    
    
    
}
