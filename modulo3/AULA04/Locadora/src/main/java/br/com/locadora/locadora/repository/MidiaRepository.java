/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.repository;

import br.com.locadora.locadora.entity.Aluguel;
import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.entity.MidiaType;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    Page<Midia> findByIdFilmeTituloContainingIgnoreCaseOrIdFilmeCategoriaOrIdFilmeLancamentoBetween(String titulo, Categoria categoria, LocalDate ini, LocalDate fim, Pageable pageable);
    
    List<Midia> findByIdFilmeAndMidiaType(Filme filme, MidiaType tipo);
    
    List<Midia> findByIdFilmeAndMidiaTypeAndIdAluguel(Filme filme, MidiaType tipo, Aluguel idAluguel);
    
    Midia findFirstByOrderByIdAluguelPrevisao();
    
    Midia findFirstByMidiaType(MidiaType midiaType);
    
    List<Midia> findByMidiaType(MidiaType midiaType);
    
    Long countByMidiaType(MidiaType midiaType);
    
    List<Midia> findByIdFilme(Filme idFilme);
    
    List<Midia> findByIdAluguel(Aluguel idAluguel);
    
    Long countByMidiaTypeAndIdFilme(MidiaType midiaType, Filme filme);
    
    Midia findFirstByIdFilmeAndMidiaType(Filme idFilme, MidiaType midiaType);

}
