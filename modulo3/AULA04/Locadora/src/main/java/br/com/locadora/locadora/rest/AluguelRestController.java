/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.entity.Aluguel;
import br.com.locadora.locadora.dto.AluguelDTO;
import br.com.locadora.locadora.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas
 */
@RestController
@RequestMapping("/api/aluguel")
public class AluguelRestController extends AbstractRestController<Aluguel> {
    @Autowired
    private AluguelService service;

    public AluguelService getService() {
        return service;
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/retirada")
    public ResponseEntity<?> retirada(@RequestBody AluguelDTO aluguelDTO){
        if (aluguelDTO.getIdCliente() == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(getService().retirar(aluguelDTO));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/devolucao")
    public ResponseEntity<?> devolucao(@RequestBody AluguelDTO aluguel) {
        return ResponseEntity.ok(getService().devolucao(aluguel.getMidias()));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @GetMapping("/devolucao")
    public ResponseEntity<?> devolucaoHoje(Pageable pageable) {
        return ResponseEntity.ok(getService().getDevolucaoHoje(pageable));
    }

    
}
