/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.repository;

import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.entity.ValorMidia;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author douglas
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {
    
    ValorMidia findFirstByIdMidiaAndFinalVigencia(Midia midia, ZonedDateTime finalVigencia);
    
    List<ValorMidia> findByIdMidia(Midia midia);
    
}
