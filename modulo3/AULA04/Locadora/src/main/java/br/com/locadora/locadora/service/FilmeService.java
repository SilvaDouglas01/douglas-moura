/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.dto.CountDTO;
import br.com.locadora.locadora.dto.FilmeCatalogoDTO;
import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.entity.ValorMidia;
import br.com.locadora.locadora.dto.FilmeDTO;
import br.com.locadora.locadora.dto.MidiaCatalogoDTO;
import br.com.locadora.locadora.dto.PrecoDTO;
import br.com.locadora.locadora.entity.Aluguel;
import br.com.locadora.locadora.repository.FilmeRepository;
import br.com.locadora.locadora.repository.MidiaRepository;
import br.com.locadora.locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas
 */
@Service
public class FilmeService extends AbstractCrudService {

    @Autowired
    private FilmeRepository repository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Autowired
    private FilmeService filmeService;

    @Override
    public FilmeRepository getRepository() {
        return repository;
    }

    @Override
    public Optional<Filme> findById(Long id) {
        return getRepository().findById(id);
    }

    public CountDTO countByTipo(Long idFilme, MidiaType tipo) {
        Long count = midiaRepository.countByMidiaTypeAndIdFilme(tipo, filmeService.findById(idFilme).get());
        return CountDTO.builder()
                .count(count)
                .build();
    }

    public Page<Filme> search(String titulo, Categoria categoria, LocalDate ini, LocalDate fim, Pageable pageable) {
        if (ini == null) {
            ini = LocalDate.MIN;
        }
        if (fim == null) {
            fim = LocalDate.MIN;
        }
        return getRepository().findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(titulo, categoria, ini, fim, pageable);

    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme saveDTO(@NotNull @Valid FilmeDTO filmeDTO) {

        Filme filme = Filme.builder()
                .categoria(filmeDTO.getCategoria())
                .lancamento(filmeDTO.getLancamento())
                .titulo(filmeDTO.getTitulo())
                .build();

        getRepository().save(filme);
        filmeDTO.setId(filme.getId());

        filmeDTO.getMidia().forEach((midia) -> {
            for (int y = 0; y < midia.getQuantidade(); y++) {
                Midia midia_ = midiaService.saveMidiaDTO(midia, filme);

                ValorMidia valorMidia = ValorMidia.builder()
                        .inicioVigencia(ZonedDateTime.now())
                        .idMidia(midia_)
                        .valor(midia.getValor())
                        .build();

                valorMidiaService.save(valorMidia);
                midiaService.save(midia_);
            }
        });

        return getRepository().save(filme);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Page<PrecoDTO> getPrecos(@NotNull @Valid Long id, Pageable pageable) {
        Filme filme = filmeService.findById(id).get();
        List<PrecoDTO> precos = new ArrayList<>(Arrays.asList());
        midiaRepository.findByIdFilme(filme).forEach((midia) -> {
            valorMidiaRepository.findByIdMidia(midia).stream().map((valorMidia) -> PrecoDTO.builder()
                    .id(valorMidia.getId())
                    .tipo(midia.getMidiaType())
                    .valor(valorMidia.getValor())
                    .inicioVigencia(valorMidia.getInicioVigencia())
                    .finalVigencia(valorMidia.getFinalVigencia())
                    .build()).forEachOrdered((preco) -> {
                        precos.add(preco);
            });
        });
        return new PageImpl<>(precos, pageable, precos.size());
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme updateFilme(Long id, FilmeDTO filmeDTO) {

        Filme filme = Filme.builder()
                .id(id)
                .titulo(filmeDTO.getTitulo())
                .categoria(filmeDTO.getCategoria())
                .lancamento(filmeDTO.getLancamento())
                .build();
        Filme filmeAntigo = filmeService.findById(id).get();
        List<Midia> midiasAntigas = midiaRepository.findByIdFilme(filmeAntigo);
        filmeDTO.getMidia().forEach((midia) -> {
            MidiaType tipo = midia.getTipo();
            List<Midia> midiasPorTipo = new ArrayList<>(midiasAntigas);
            midiasPorTipo.removeIf(m -> !m.getMidiaType().equals(tipo));
            midiasPorTipo.stream().map((midiaPorTipo) -> {
                ValorMidia valorAntigo = valorMidiaRepository.findFirstByIdMidiaAndFinalVigencia(midiaPorTipo, null);
                valorAntigo.setFinalVigencia(ZonedDateTime.now());
                ValorMidia valorMidia = ValorMidia.builder()
                        .idMidia(midiaPorTipo)
                        .inicioVigencia(ZonedDateTime.now())
                        .valor(midia.getValor())
                        .build();
                return valorMidia;
            }).forEachOrdered((valorMidia) -> {
                valorMidiaRepository.save(valorMidia);
            });
            if (midiasPorTipo.size() > midia.getQuantidade()) {
                for (int diferenca = midiasPorTipo.size() - midia.getQuantidade(); diferenca != 0; diferenca--) {
                    Midia midiaAux = midiaRepository.findFirstByIdFilmeAndMidiaType(filmeAntigo, tipo);
                    List<ValorMidia> valores = valorMidiaRepository.findByIdMidia(midiaAux);
                    valores.forEach(m -> valorMidiaRepository.delete(m));
                    midiaService.delete(midiaRepository.findFirstByIdFilmeAndMidiaType(filmeAntigo, tipo).getId());
                }
            } else if (midiasPorTipo.size() < midia.getQuantidade()) {

                for (int diferenca = midia.getQuantidade() - midiasPorTipo.size(); diferenca != 0; diferenca--) {
                    Midia midiaNova = Midia.builder()
                            .idFilme(filmeAntigo)
                            .midiaType(tipo)
                            .build();
                    ValorMidia valorMidia = ValorMidia.builder()
                            .inicioVigencia(ZonedDateTime.now())
                            .idMidia(midiaNova)
                            .valor(midia.getValor())
                            .build();
                    valorMidiaService.save(valorMidia);
                    midiaService.save(midiaNova);
                }
            }
        });

        return getRepository().save(filme);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Page<FilmeCatalogoDTO> searchCatalogo(String titulo, Pageable pageable) {
        List<Filme> filmes = repository.findByTituloIgnoreCaseContaining(titulo);
        List<FilmeCatalogoDTO> filmesCatalogados = new ArrayList<>();
        List<MidiaCatalogoDTO> midias = new ArrayList<>();

        filmes.stream().map((filme) -> {
            FilmeCatalogoDTO filme_ = FilmeCatalogoDTO.builder()
                    .titulo(filme.getTitulo())
                    .categoria(filme.getCategoria())
                    .lancamento(filme.getLancamento())
                    .build();
            for (MidiaType tipo : MidiaType.values()) {
                if (!midiaRepository.findByIdFilmeAndMidiaType(filme, tipo).isEmpty()) {
                    Midia midiaAux = midiaRepository.findFirstByMidiaType(tipo);
                    if (midiaRepository.findByIdFilmeAndMidiaTypeAndIdAluguel(filme, tipo, null).isEmpty()) {
                        MidiaCatalogoDTO midia = MidiaCatalogoDTO.builder()
                                .tipo(tipo)
                                .previsao(findAluguelMaisRecente(filme, tipo).getPrevisao())
                                .preco(valorMidiaRepository.findFirstByIdMidiaAndFinalVigencia(midiaAux, null).getValor())
                                .disponivel(false)
                                .build();
                        midias.add(midia);
                    } else {
                        MidiaCatalogoDTO midia = MidiaCatalogoDTO.builder()
                                .preco(valorMidiaRepository.findFirstByIdMidiaAndFinalVigencia(midiaAux, null).getValor())
                                .tipo(tipo)
                                .disponivel(true)
                                .build();
                        midias.add(midia);
                    }
                }
            }
            filme_.setMidias(midias);
            return filme_;
        }).forEachOrdered((filme_) -> {
            filmesCatalogados.add(filme_);
        });

        return new PageImpl<>(filmesCatalogados, pageable, filmesCatalogados.size());
    }

    public Aluguel findAluguelMaisRecente(Filme filme, MidiaType tipo) {
        List<Midia> midias = midiaRepository.findByIdFilmeAndMidiaType(filme, tipo);
        List<Aluguel> alugueis = new ArrayList<>();
        midias.forEach(m -> alugueis.add(m.getIdAluguel()));
        Aluguel aluguelRetorno = Aluguel.builder()
                .previsao(LocalDate.MAX)
                .build();
        for (Aluguel aluguel : alugueis) {
            if (aluguel.getPrevisao().isBefore(aluguelRetorno.getPrevisao())) {
                aluguelRetorno = aluguel;
            }
        }

        return aluguelRetorno;
    }

}
