/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.dto;

import br.com.locadora.locadora.entity.MidiaType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MidiaDTO {
    
    private MidiaType tipo;
    
    private int quantidade;
    
    private Double valor;
            
    
}
