/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.entity.ValorMidia;
import br.com.locadora.locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas
 */
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/valormidia")
public class ValorMidiaRestController extends AbstractRestController<ValorMidia> {

    @Autowired
    private ValorMidiaService service;
    

    @Override
    public ValorMidiaService getService() {
        return service;
    }
    
    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(getService().findAll(pageable));
    }
    
}
