/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MIDIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Midia.findAll", query = "SELECT m FROM Midia m")
    , @NamedQuery(name = "Midia.findById", query = "SELECT m FROM Midia m WHERE m.id = :id")
    , @NamedQuery(name = "Midia.findByMidiaType", query = "SELECT m FROM Midia m WHERE m.midiaType = :midiaType")})
public class Midia extends AbstractEntity implements Serializable {

   
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "MIDIA_TYPE")
    @Enumerated(value = EnumType.STRING)
    private MidiaType midiaType;
    @JoinColumn(name = "ID_ALUGUEL", referencedColumnName = "ID")
    @ManyToOne
    private Aluguel idAluguel;
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne
    @NotNull
    private Filme idFilme;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idMidia")
    //@JsonIgnore
    //private List<ValorMidia> valorMidiaList;
    
}
