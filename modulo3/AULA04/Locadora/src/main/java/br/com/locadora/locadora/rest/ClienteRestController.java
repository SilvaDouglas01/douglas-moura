/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.entity.Cliente;
import br.com.locadora.locadora.service.ClienteService;
import br.com.locadora.locadora.service.CorreiosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author douglas
 */
@RestController
@RequestMapping("/api/cliente")
public class ClienteRestController extends AbstractRestController<Cliente> {

    @Autowired
    private ClienteService service;
    
    @Autowired
    private CorreiosService correiosService;

    @Override
    public ClienteService getService() {
        return service;
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> cep(@PathVariable("cep") String cep) {
        return ResponseEntity.ok(correiosService.consultaCEP(cep));
    }
    
}
