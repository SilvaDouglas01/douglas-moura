package br.com.locadora.locadora.service;

import br.com.locadora.locadora.domain.User;
import br.com.locadora.locadora.dto.UserDTO;
import br.com.locadora.locadora.repository.RoleRepository;
import br.com.locadora.locadora.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by nydiarra on 06/05/17.
 */
@Component
public class AppUserDetailsService extends AbstractCrudService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        });

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);

        return userDetails;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User create(UserDTO account) {
        User user = new User();
        user.setFirstName(account.getFirstName());
        user.setLastName(account.getLastName());
        user.setUsername(account.getUsername());
        user.setRoles(Arrays.asList(roleRepository.findByRoleName("STANDARD_USER")));
        user.setPassword(passwordEncoder.encode(account.getPassword()));
        return userRepository.save(user);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User updatePassword(UserDTO account){
        User user = userRepository.findByUsername(account.getUsername());
        user.setPassword(passwordEncoder.encode(account.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public JpaRepository getRepository() {
        return userRepository;
    }

}
