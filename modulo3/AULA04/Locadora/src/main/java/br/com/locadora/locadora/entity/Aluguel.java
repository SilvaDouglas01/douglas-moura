/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author douglas
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ALUGUEL")
@NamedQueries({
    @NamedQuery(name = "Aluguel.findAll", query = "SELECT a FROM Aluguel a")
    , @NamedQuery(name = "Aluguel.findById", query = "SELECT a FROM Aluguel a WHERE a.id = :id")
    , @NamedQuery(name = "Aluguel.findByRetirada", query = "SELECT a FROM Aluguel a WHERE a.retirada = :retirada")
    , @NamedQuery(name = "Aluguel.findByPrevisao", query = "SELECT a FROM Aluguel a WHERE a.previsao = :previsao")
    , @NamedQuery(name = "Aluguel.findByDevolucao", query = "SELECT a FROM Aluguel a WHERE a.devolucao = :devolucao")
    , @NamedQuery(name = "Aluguel.findByMulta", query = "SELECT a FROM Aluguel a WHERE a.multa = :multa")})
public class Aluguel extends AbstractEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "RETIRADA")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull
    private LocalDate retirada;
    @Column(name = "PREVISAO")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate previsao;
    @Column(name = "DEVOLUCAO")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    private ZonedDateTime devolucao;
    @Column(name = "MULTA")
    private Double multa;
    @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID")
    @ManyToOne
    @JsonIgnore
    @NotNull
    private Cliente idCliente;
   // @OneToMany(mappedBy = "idAluguel")
    //@JsonIgnore
    //private List<Midia> midiaList;
    @Column(name = "PRECO")
    private Double preco;

    
    
}
