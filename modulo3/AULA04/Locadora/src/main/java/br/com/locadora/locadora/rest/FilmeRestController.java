/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

import br.com.locadora.locadora.entity.Categoria;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.entity.MidiaType;
import br.com.locadora.locadora.dto.FilmeDTO;
import br.com.locadora.locadora.service.FilmeService;
import java.time.LocalDate;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author douglas
 */
@RestController
@RequestMapping("/api/filme")
public class FilmeRestController extends AbstractRestController<Filme> {

    @Autowired
    private FilmeService service;

    @Override
    public FilmeService getService() {
        return service;
    }

    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PostMapping("/midia")
    public ResponseEntity<?> post(@RequestBody FilmeDTO input) {
        return ResponseEntity.ok(getService().saveDTO(input));
    }

    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @PutMapping("/{id}/midia")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody FilmeDTO input) {
        if (id == null || !Objects.equals(input.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(getService().updateFilme(id, input));
    }

    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @GetMapping("/precos/{id}")
    public ResponseEntity precos(@PathVariable Long id, Pageable pageable) {
        return ResponseEntity.ok(getService().getPrecos(id, pageable));
    }

    @PreAuthorize("hasAuthority('ADMIN_USER') OR hasAuthority('STANDARD_USER')")
    @GetMapping("/search")
    public ResponseEntity<?> search(Pageable pageable,
            @RequestParam(name = "titulo", required = false) String titulo,
            @RequestParam(name = "categoria", required = false) Categoria categoria,
            @RequestParam(name = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(name = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoFim) {
        return ResponseEntity.ok(getService().search(titulo, categoria, lancamentoIni, lancamentoFim, pageable));
    }

    @PreAuthorize("hasAuthority('ADMIN_USER')")
    @GetMapping("/count/{id}/{midiaType}")
    public ResponseEntity<?> countByTipo(@PathVariable Long id, @PathVariable MidiaType midiaType) {
        return ResponseEntity.ok(getService().countByTipo(id, midiaType));
    }

    @PreAuthorize("hasAuthority('ADMIN_USER') OR hasAuthority('STANDARD_USER')")
    @PostMapping("/search/catalogo")
    public ResponseEntity<?> catalogo(Pageable pageable, @RequestBody FilmeDTO input) {
        return ResponseEntity.ok(getService().searchCatalogo(input.getTitulo(), pageable));
    }
}
