/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.config.SoapConnector;
import br.com.locadora.locadora.dto.EnderecoDTO;
import br.com.locadora.locadora.ws.ConsultaCEP;
import br.com.locadora.locadora.ws.ConsultaCEPResponse;
import br.com.locadora.locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author douglas
 */
@Service
public class CorreiosService {

    @Autowired
    private SoapConnector soapConnector;

    @Autowired
    private ObjectFactory objectFactory;

    private final String CORREIOS_URL = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";

    public EnderecoDTO consultaCEP(String cep) {
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);

        ConsultaCEPResponse resultado = ((JAXBElement<ConsultaCEPResponse>) soapConnector
                .callWebService(CORREIOS_URL, objectFactory.createConsultaCEP(consulta))).getValue();

        EnderecoDTO endereco = EnderecoDTO.builder()
                .bairro(resultado.getReturn().getBairro())
                .cidade(resultado.getReturn().getCidade())
                .estado(resultado.getReturn().getUf())
                .rua(resultado.getReturn().getEnd())
                .build();
        return endereco;
    }
}
