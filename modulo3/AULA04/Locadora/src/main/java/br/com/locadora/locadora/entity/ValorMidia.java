/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author douglas
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "VALOR_MIDIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValorMidia.findAll", query = "SELECT v FROM ValorMidia v")
    , @NamedQuery(name = "ValorMidia.findById", query = "SELECT v FROM ValorMidia v WHERE v.id = :id")
    , @NamedQuery(name = "ValorMidia.findByValor", query = "SELECT v FROM ValorMidia v WHERE v.valor = :valor")
    , @NamedQuery(name = "ValorMidia.findByInicioVigencia", query = "SELECT v FROM ValorMidia v WHERE v.inicioVigencia = :inicioVigencia")
    , @NamedQuery(name = "ValorMidia.findByFinalVigencia", query = "SELECT v FROM ValorMidia v WHERE v.finalVigencia = :finalVigencia")})
public class ValorMidia extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "VALOR")
    private Double valor;
    @Column(name = "INICIO_VIGENCIA")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    private ZonedDateTime inicioVigencia;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(name = "FINAL_VIGENCIA")
    private ZonedDateTime finalVigencia;
    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne
    private Midia idMidia;

}
