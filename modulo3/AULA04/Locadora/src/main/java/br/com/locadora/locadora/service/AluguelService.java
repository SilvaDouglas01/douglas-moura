/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.entity.Aluguel;
import br.com.locadora.locadora.entity.Cliente;
import br.com.locadora.locadora.entity.Filme;
import br.com.locadora.locadora.entity.Midia;
import br.com.locadora.locadora.dto.AluguelDTO;
import br.com.locadora.locadora.repository.AluguelRepository;
import br.com.locadora.locadora.repository.MidiaRepository;
import br.com.locadora.locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas
 */
@Service
public class AluguelService extends AbstractCrudService {

    @Autowired
    private AluguelRepository repository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private AluguelService aluguelService;

    @Autowired
    private LocalDateService localDateService;

    @Autowired
    private ClienteService clienteService;

    @Override
    public AluguelRepository getRepository() {
        return repository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Aluguel retirar(AluguelDTO aluguelDTO) {
        Aluguel aluguel = Aluguel.builder()
                .retirada(LocalDate.now())
                .idCliente((Cliente) clienteService.findById(aluguelDTO.getIdCliente()).get())
                .build();

        List<Midia> midias = new ArrayList<>();

        getRepository().save(aluguel);

        for (int i = 0; i < aluguelDTO.getMidias().size(); i++) {
            Midia midia = (Midia) midiaService.findById(aluguelDTO.getMidias().get(i)).get();
            midias.add(midia);
            midia.setIdAluguel(aluguel);
            midiaService.save(midia);
        }

        aluguel.setPrevisao(LocalDate.now().plusDays(midias.size()));
        Double preco = 0.0;
        for (Midia midia : midias) {
            preco = Double.sum(preco, valorMidiaRepository.findFirstByIdMidiaAndFinalVigencia(midia, null).getValor());
        }
        aluguel.setPreco(preco);
        return getRepository().save(aluguel);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Aluguel devolucao(List<Long> idMidias) {
        Midia primeiraMidia = (Midia) midiaService.findById(idMidias.get(0)).get();
        Long idAluguel = primeiraMidia.getIdAluguel().getId();
        Aluguel aluguel = (Aluguel) aluguelService.findById(idAluguel).get();
        List<Midia> midias = midiaRepository.findByIdAluguel(aluguel);
        List<Midia> midiasRemover = new ArrayList<>();
        idMidias.stream().map((idMidia) -> {
            midias.stream().filter((midia) -> (midia.getId().equals(idMidia))).forEachOrdered((midia) -> {
                midiasRemover.add(midia);
            });
            return idMidia;
        }).map((idMidia) -> (Midia) midiaService.findById(idMidia).get()).map((midia) -> {
            midia.setIdAluguel(null);
            return midia;
        }).map((midia) -> {
            return midia;
        }).forEachOrdered((midia) -> {
            midiaService.save(midia);
        });
        midias.removeAll(midiasRemover);
        double multa = 0;
        if (midias.isEmpty()) {
            aluguel.setDevolucao(ZonedDateTime.now());
            if ((localDateService.ZonedNow().toLocalDate().equals(aluguel.getPrevisao()) && localDateService.ZonedNow().getHour() >= 16)
                    || (localDateService.ZonedNow().toLocalDate().isAfter(aluguel.getPrevisao()))) {
                multa = aluguel.getPreco();
            }

        }
        aluguel.setMulta(multa);
        return getRepository().save(aluguel);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Page<Filme> getDevolucaoHoje(Pageable pageable) {
        List<Aluguel> alugueisHoje = getRepository().findByPrevisao(localDateService.now());
        List<Filme> filmes = new ArrayList<>();
        alugueisHoje.forEach((aluguel) -> {
            midiaRepository.findByIdAluguel(aluguel).forEach((midia) -> {
                filmes.add(midia.getIdFilme());
            });
        });

        return new PageImpl<>(filmes, pageable, filmes.size());
    }
}
