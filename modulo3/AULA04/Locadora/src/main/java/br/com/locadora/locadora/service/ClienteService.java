/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.service;

import br.com.locadora.locadora.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author douglas
 */
@Service
public class ClienteService extends AbstractCrudService {

    @Autowired
    private ClienteRepository repository;
    
    @Override
    public ClienteRepository getRepository() {
        return repository;
    }
    
    
}
