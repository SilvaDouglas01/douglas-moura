/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.locadora.locadora.rest;

/**
 *
 * @author douglas
 */

import br.com.locadora.locadora.dto.UserDTO;
import br.com.locadora.locadora.service.AppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@PreAuthorize("hasAuthority('ADMIN_USER')")
@RestController
@RequestMapping("/api/user")
public class UserRestController{

    @Autowired
    private AppUserDetailsService userService;
    
    public AppUserDetailsService getService() {
        return userService;
    }
    
    @PostMapping
    public ResponseEntity<?> create(@RequestBody UserDTO user){
        return ResponseEntity.ok(getService().create(user));
    }
    
    @PostMapping("/password")
    public ResponseEntity<?> changePassword(@RequestBody UserDTO user){
        return ResponseEntity.ok(getService().updatePassword(user));
    }
    
    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(getService().findAll(pageable));
    }
    
    
}
