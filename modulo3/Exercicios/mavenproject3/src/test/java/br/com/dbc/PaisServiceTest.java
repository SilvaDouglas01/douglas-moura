/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import br.com.dbc.entity.Pais;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;

/**
 *
 * @author douglas.silva
 */
public class PaisServiceTest {
    
    public PaisServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of buscarPaisPorNome method, of class PaisService.
     */
    @org.junit.Test
    public void testBuscarPaisPorNome() {
        System.out.println("buscarPaisPorNome");
        String nome = "%razil";
        PaisService instance = PaisService.getInstance();
        String expNome = "Brazil";
        List<Pais> result = instance.buscarPaisPorNome(nome);
        assertEquals(1, result.size());
        assertEquals(expNome, result.get(0).getNome());
    }
    
    @org.junit.Test
    public void testBuscarPaisPorSigla() {
        System.out.println("buscarPaisPorSigla");
        String sigla = "BRA";
        PaisService instance = PaisService.getInstance();
        String expSigla = "BRA";
        List<Pais> result = instance.buscarPaisPorSigla(sigla);
        assertEquals(1, result.size());
        assertEquals(expSigla, result.get(0).getSigla());
    }
    
    @org.junit.Test
    public void testBuscarPaisPorID() {
        System.out.println("buscarPaisPorID");
        Long id = new Long(2);
        PaisService instance = PaisService.getInstance();
        Long expId = new Long(2);
        List<Pais> result = instance.buscarPaisPorId(id);
        assertEquals(1, result.size());
        assertEquals(expId, result.get(0).getId());
    }  
}
