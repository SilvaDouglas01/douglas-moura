/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import static br.com.dbc.PersistenceUtils.getEm;
import br.com.dbc.entity.Pais;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author douglas.silva
 */
public class PaisService {
    
    private static PaisService instance;
    
    public static PaisService getInstance(){
        return instance == null ? new PaisService() : instance;
    }
    
    
    private PaisService(){
        
    }
    
    public List<Pais> buscarPaisPorNome(String nome){
        return getEm()
                .createQuery("select p from Pais p where p.nome like :nome", Pais.class)
                .setParameter("nome", nome)
                .getResultList();
    }
    
    public List<Pais> buscarPaisPorSigla(String sigla){
        return getEm()
                .createQuery("select p from Pais p where p.sigla like :sigla", Pais.class)
                .setParameter("sigla", sigla)
                .getResultList();
    }
    
    public List<Pais> buscarPaisPorId(Long id){
        return getEm()
                .createQuery("select p from Pais p where p.id like :id", Pais.class)
                .setParameter("id", id)
                .getResultList();
    }
}
