/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author douglas.silva
 */
public class Main {
    
    public static void main(String ... args){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("dia2_pu");
        EntityManager em = emf.createEntityManager();
    }
    
}
