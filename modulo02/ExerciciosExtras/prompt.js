const meuH2 = document.getElementById( 'tituloPagina' )
verificarBotao()
function perguntarNome() {
  const nome = prompt( 'Qual seu nome?' )
  meuH2.innerText = nome
  localStorage.nome = nome
}

function renderizaNomeArmazenadoNaTela() {
  meuH2.innerText = localStorage.nome
}

const nomeArmazenado = localStorage.nome && localStorage.nome.length > 0
if ( nomeArmazenado ) {
  renderizaNomeArmazenadoNaTela()
} else {
  perguntarNome()
}

function removerNomeStorage(){
  localStorage.removeItem("nome")
  localStorage.numeroDeCliques++
  verificarBotao()
}

function verificarBotao(){
  if (localStorage.numeroDeCliques>=5)
    document.querySelector("button").disabled = true
}

if (!localStorage.numeroDeCliques){
  localStorage.numeroDeCliques = 0
}
