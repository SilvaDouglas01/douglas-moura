function main(){
  const btnPararRelogio = document.getElementById('btnPararRelogio')
  const btnReiniciarRelogio = document.getElementById('btnReiniciarRelogio')
  class Relogio{
    constructor(){
      this.horario = document.getElementById('horario')
      this.horario.innerText = new Date().toLocaleTimeString()
      this.idInterval = setInterval(this.atualizar, 1000)
      btnReiniciarRelogio.disabled = true  
    }
  
    atualizar(){
      this.horario.innerText = new Date().toLocaleTimeString() 
    }
    
    pausar(){
      clearInterval(this.idInterval)
      btnPararRelogio.disabled = true
      btnReiniciarRelogio.disabled = false
    }

    reiniciar(){
      this.atualizar()
      this.idInterval = setInterval(this.atualizar, 1000)
      btnPararRelogio.disabled = false
      btnReiniciarRelogio.disabled = true
    }
  }
  const relogio = new Relogio()
  btnPararRelogio.onclick = function () {
    relogio.pausar()
  }
  btnReiniciarRelogio.onclick = function () {
    relogio.reiniciar()
  }
}
main()


