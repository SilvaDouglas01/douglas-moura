function somarPosicoesPares(listaNumeros){
  var soma = 0
  for(var i=0; i<listaNumeros.length; i+=2){
    soma+= listaNumeros[i]
  }
  return soma
}



function formatarElfos(listaElfos){

  for(var x = 0; x<listaElfos.length;x++){
    listaElfos[x].nome = listaElfos[x].nome.toUpperCase()
    listaElfos[x].experiencia > 0 ? listaElfos[x].temExperiencia = true : listaElfos[x].temExperiencia = false
    delete listaElfos[x].experiencia
    listaElfos[x].descricao = (listaElfos[x].nome+(stringIntermediaria(listaElfos[x]))+(listaElfos[x].qtdFlechas>0? listaElfos[x].qtdFlechas+" " : "")+flexãoPalavraFlecha(listaElfos[x]))
  }

  function stringIntermediaria(elfo){
    if(elfo.temExperiencia && elfo.qtdFlechas>0)
      return " com experiencia e "
    else if(elfo.temExperiencia && elfo.qtdFlechas===0)
      return " com experiencia e sem "
    else if(!elfo.temExperiencia && elfo.qtdFlechas>0)
      return " sem experiencia e com "
    else if(!elfo.temExperiencia && elfo.qtdFlechas===0)
      return " sem experiencia e "  
 }

  function flexãoPalavraFlecha(elfo){
    if(elfo.qtdFlechas===1)
      return "flecha"
    else if(elfo.qtdFlechas>1 || elfo.qtdFlechas===0)
      return "flechas"
  }
  return listaElfos
}
