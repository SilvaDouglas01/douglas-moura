describe( 'somarPosicoesPares', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'somar positivos', function() {
    const listaNumeros = [1, 2.1, 3.3, 6]
    const resultado = somarPosicoesPares(listaNumeros)
    resultado.should.equal(4.3)
  } )

  it( 'somar positivos e negativos', function() {
    const listaNumeros = [ 1, 56, 4.34, 6, -2 ] 
    const resultado = somarPosicoesPares(listaNumeros)
    resultado.should.equal(3.34)
  } )

  it( 'somar 1 numero', function() {
    const listaNumeros = [6.5] 
    const resultado = somarPosicoesPares(listaNumeros)
    resultado.should.equal(6.5)
  } )

  it( 'somar array vazio', function() {
    const listaNumeros = [] 
    const resultado = somarPosicoesPares(listaNumeros)
    resultado.should.equal(0)
  } )

} )

describe( 'formatarElfos', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'sem experiencia e com flechas/com experiencia e com 1 flecha', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
  	const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const elfo1expect = { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 6, descricao: "LEGOLAS sem experiencia e com 6 flechas" }
    const elfo2expect = { nome: "GALADRIEL", temExperiencia: true, qtdFlechas: 1, descricao: "GALADRIEL com experiencia e 1 flecha" }
    const resultado = [elfo1expect, elfo2expect]
    chai.expect(resultado).to.eql(formatarElfos([elfo1, elfo2]))
  } )

  it( 'sem experiencia e 1 flecha/com experiencia e sem flechas', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 1 }
  	const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 0 }
    const elfo1expect = { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 1, descricao: "LEGOLAS sem experiencia e com 1 flecha" }
    const elfo2expect = { nome: "GALADRIEL", temExperiencia: true, qtdFlechas: 0, descricao: "GALADRIEL com experiencia e sem flechas" }
    const resultado = [elfo1expect, elfo2expect]
    chai.expect(resultado).to.eql(formatarElfos([elfo1, elfo2]))
  } )

  it( 'sem experiencia e sem flechas/com experiencia e com flechas', function() {
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 0 }
  	const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 5 }
    const elfo1expect = { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 0, descricao: "LEGOLAS sem experiencia e flechas" }
    const elfo2expect = { nome: "GALADRIEL", temExperiencia: true, qtdFlechas: 5, descricao: "GALADRIEL com experiencia e 5 flechas" }
    const resultado = [elfo1expect, elfo2expect]
    chai.expect(resultado).to.eql(formatarElfos([elfo1, elfo2]))
  } )

} )
