describe( 'calcularCirculo', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'deve calcular área de raio 3', function() {
    const raio = 3
    const tipoCalculo = "A"
    const resultado = calcularCirculo({raio, tipoCalculo})
    resultado.should.equal(28.274333882308138)
  } )

  it( 'deve calcular circunferencia de raio 3', function() {
    const raio = 3
    const tipoCalculo = "C"
    const resultado = calcularCirculo({raio, tipoCalculo})
    resultado.should.equal(18.84955592153876)
  } )
} )

describe( 'naoBissexto', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'deve retornar false para 2016', function() {
    const resultado = naoBissexto(2016)
    resultado.should.equal(false)
  } )

  it( 'deve retornar true para 2017', function() {
    const resultado = naoBissexto(2017)
    resultado.should.equal(true)
  } )

  it( 'deve retornar false para 2000', function() {
    const resultado = naoBissexto(2000)
    resultado.should.equal(false)
  } )

  it( 'deve retornar true para 1500', function() {
    const resultado = naoBissexto(1500)
    resultado.should.equal(true)
  } )
} )

describe( 'concatenarSemUndefined', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'concatenar com undefined no primeiro parametro', function() {
    const resultado = concatenarSemUndefined( undefined, "Soja" ) 
    resultado.should.equal("Soja")
  } )

  it( 'concatenar com 2 parametros bem definidos', function() {
    const resultado = concatenarSemUndefined( "Soja", " é bom" ) 
    resultado.should.equal("Soja é bom")
  } )

  it( 'concatenar com apenas 1 parametro', function() {
    const resultado = concatenarSemUndefined("Soja" ) 
    resultado.should.equal("Soja")
  } )
} )

describe( 'concatenarSemNull', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'concatenar com null no primeiro parametro', function() {
    const resultado = concatenarSemNull( null, "Soja" ) 
    resultado.should.equal("Soja")
  } )

  it( 'concatenar com 2 parametros bem definidos', function() {
    const resultado = concatenarSemNull( "Soja", " é bom" ) 
    resultado.should.equal("Soja é bom")
  } )

  it( 'concatenar com apenas 1 parametro', function() {
    const resultado = concatenarSemNull("Soja é") 
    resultado.should.equal("Soja éundefined")
  } )
} )

describe( 'concatenarEmPaz', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'concatenar com undefined no primeiro parametro', function() {
    const resultado = concatenarEmPaz( undefined, "Soja" ) 
    resultado.should.equal("Soja")
  } )

  it( 'concatenar com null no segundo parametro', function() {
    const resultado = concatenarEmPaz( "Soja é bom mas...", null ) 
    resultado.should.equal("Soja é bom mas...")
  } )

  it( 'concatenar com apenas 1 parametro', function() {
    const resultado = concatenarEmPaz("Soja" ) 
    resultado.should.equal("Soja")
  } )
} )

describe( 'adicionar', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'somar dois numeros pequenos', function() {
    const resultado = adicionar(3)(4) 
    resultado.should.equal(7)
  } )

  it( 'somar dois numeros grandes', function() {
    const resultado = adicionar(5642)(8749) 
    resultado.should.equal(14391)
  } )

  it( 'somar dois números com 1 deles sendo negativo', function() {
    const resultado = adicionar(-3)(4) 
    resultado.should.equal(1)
  } )

} )

describe( 'fiboSum', function() {
  beforeEach( function() {
    chai.should()
  } )

  it( 'soma dos primeiros 7 termos da sequência', function() {
    const resultado = fiboSum(7) 
    resultado.should.equal(33)
  } )

  it( 'soma dos primeiros 30 termos da sequência', function() {
    const resultado = fiboSum(30) 
    resultado.should.equal(2178308)
  } )
} )
