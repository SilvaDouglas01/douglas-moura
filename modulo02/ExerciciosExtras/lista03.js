function calcularCirculo(circulo){
  if(circulo.tipoCalculo==="A")
    return Math.PI*Math.pow(circulo.raio, 2)
  if(circulo.tipoCalculo==="C")
    return 2*Math.PI*circulo.raio
}

function naoBissexto(ano){
  if(ano%4==0){
    if(ano%100!=0)
      return false
    if(ano%400==0)
      return false
  }
  return true
}

function concatenarSemUndefined(expr, expr2){
  var concatenacao = ""
  typeof expr == "undefined" ? concatenacao+="" : concatenacao+= expr
  typeof expr2 == "undefined" ? concatenacao+="" : concatenacao+= expr2
  return concatenacao 
}

function concatenarSemNull(expr, expr2){
  var concatenacao = ""
  typeof expr == "undefined" ? expr = "undefined" : null
  typeof expr2 == "undefined" ? expr2 = "undefined" : null
  expr ? concatenacao+=expr : concatenacao+= ""
  expr2 ? concatenacao+=expr2 : concatenacao+= ""
  return concatenacao 
}

function concatenarEmPaz(expr, expr2){
  var concatenacao = ""
  expr ? concatenacao+=expr : concatenacao+= ""
  expr2 ? concatenacao+=expr2 : concatenacao+= ""
  return concatenacao 
}


function fiboN(n){
  if (n<=1)
    return n
  else
    return fiboN(n-1) + fiboN(n-2)
}

function fiboSum(n){
  return (fiboN(n+2)-1)
}

function adicionar(x) {
  return (y) => {
      return x + y
  }
}
