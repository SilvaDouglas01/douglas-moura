describe( 'concatenarSemUndefined', function() {
  const expect = chai.expect
  beforeEach( function() {
    chai.should()
  } )

  it( 'passar undefined primeiro parâmetro', function() {
    concatenarSemUndefined( undefined, 'Soja' ).should.equal( 'Soja' )
  } )

  it( 'passar texto nos dois parâmetros', function() {
    concatenarSemUndefined( 'Soja', ' é bom' ).should.equal( 'Soja é bom' )
  } )

  it( 'passar undefined no segundo parâmetro', function() {
    concatenarSemUndefined( 'Soja é' ).should.equal( 'Soja é' )
  } )

  it( 'passar undefined nos dois parâmetros', function() {
    concatenarSemUndefined( undefined, undefined ).should.equal( '' )
  } )
} )
