<<<<<<< HEAD
pokeID = document.getElementById("pokeID") 
function loadPokemon() {
  //gerar pokemon aleatorio não repetido e armazenar no local storage
  function gerarAleatorio(){
    let naoEValido = true
    while(naoEValido){
      naoEValido = false
      ID = Math.floor(Math.random() * 802) - 1
      var PokemonGerados = JSON.parse(localStorage.PokemonGerados)
      for(var i=0;i<PokemonGerados.length; i++){
        PokemonGerados[i] == ID? naoEValido = true: null
      }
    }
    PokemonGerados.push(ID)
    localStorage.PokemonGerados = JSON.stringify(PokemonGerados)
    return ID
  }

  if ((pokeID.value == "") && !estouSorte) return
  if (pokeID.value == ID) return
  if (pokeID.value!="" && estouSorte == false)
    var ID = pokeID.value 
  else if(pokeID.value=="" && estouSorte == true)
    var ID = gerarAleatorio()
    
  document.getElementById("ID").innerText= "ID: "+ID
  let url = "https://pokeapi.co/api/v2/pokemon/"+ID+"/"
  var requisicao = fetch(url)
  
  requisicao
    .then( res => res.json())
    .then( dadosJson => {
    $h1.innerText = dadosJson.name
    $img.src = dadosJson.sprites.front_default
    $peso.innerText= "Peso: "+dadosJson.weight/10 + " kg"
    $altura.innerText= "Altura: "+dadosJson.height*10 + " cm" 
    $tipos.innerText= "Tipos:"
    for(var i=0; i<dadosJson.types.length;i++)
      $tipos.innerText+= " "+dadosJson.types[i].type.name+","
    $tipos.innerText= $tipos.innerText.substr(0,($tipos.innerText.length - 1))
    limparEstatisticas()
    for(var i=0; i<dadosJson.stats.length; i++){
      var li = document.createElement('li');
      li.innerHTML = dadosJson.stats[i].stat.name+": "+dadosJson.stats[i].base_stat;
      $estatisticas.appendChild(li);
    }
  })
  estouSorte = false
}  

function limparInput(){
  pokeID.value = ""
}

//limpar estatisticas do pokemon anterior
function limparEstatisticas(){
  while ($estatisticas.firstChild) {
    $estatisticas.removeChild($estatisticas.firstChild);
  }
}
function estouComSorte(){
  estouSorte = true
}
const $dadosPokemon = document.getElementById("dadosPokemon")
const $h1 = $dadosPokemon.querySelector("h1")
const $img = $dadosPokemon.querySelector("img")
const $peso = document.getElementById("peso")
const $altura = document.getElementById("altura")
const $tipos = document.getElementById("tipos")
const $estatisticas = document.getElementById("estatisticas")
var estouSorte = false

if(!localStorage.PokemonGerados)
  localStorage.PokemonGerados = "[]"
=======
function rodarPrograma() {

  // obtém elementos da tela para trabalhar com eles
  const $dadosPokemon = document.getElementById( 'dadosPokemon' )
  const $h1 = $dadosPokemon.querySelector( 'h1' )
  const $img = $dadosPokemon.querySelector( '#thumb' )
  const $txtIdPokemon = document.getElementById( 'txtNumero' )
  // instanciando objeto de API que criamos para facilitar a comunicação
  const url = 'https://pokeapi.co/api/v2/pokemon'
  const pokeApi = new PokeApi( url )

  // registrar evento de onblur
  $txtIdPokemon.onblur = function() {
    const idDigitado = $txtIdPokemon.value
    pokeApi.buscar( idDigitado )
      .then( res => res.json() )
      .then( dadosJson => {
        // criar objeto pokémon e renderiza-lo na tela
        const pokemon = new Pokemon( dadosJson )
        renderizarPokemonNaTela( pokemon )
      } )
  }

  function renderizarPokemonNaTela( pokemon ) {
    $h1.innerText = pokemon.nome
    $img.src = pokemon.thumbUrl
  }

}

rodarPrograma()
>>>>>>> master
