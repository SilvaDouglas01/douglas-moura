import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Login from "./components/screens/Login.vue"
import Home from "./components/screens/Home.vue"
import VeeValidate, {Validator} from 'vee-validate'
import ptBR from 'vee-validate/dist/locale/pt_BR'

Vue.config.productionTip = false

Validator.localize('pt_BR', ptBR)

Vue.use(VeeValidate)
Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [ 
  { path: '/', component: Login },
  { name: 'home', path: '/home/:usuario', component: Home}
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
