export default class Pokemon {
  constructor( jsonVindoDaApi ) {
    this.nome = jsonVindoDaApi.name
    this.id = jsonVindoDaApi.id
    this.thumbUrl = jsonVindoDaApi.sprites.front_default
    this._altura = jsonVindoDaApi.height
    this.peso = jsonVindoDaApi.weight/10
    this.tipos = jsonVindoDaApi.types.map( t => t.type.name )
    this.speed = jsonVindoDaApi.stats[0].base_stat;
    this.specialDefense = jsonVindoDaApi.stats[1].base_stat;
    this.specialAttack = jsonVindoDaApi.stats[2].base_stat;
    this.defense = jsonVindoDaApi.stats[3].base_stat;
    this.attack = jsonVindoDaApi.stats[4].base_stat;
    this.hp = jsonVindoDaApi.stats[5].base_stat;
  }
  get altura() {
    return this._altura * 10
  }
}
